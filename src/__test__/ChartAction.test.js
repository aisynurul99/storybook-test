import React from 'react';
import { render, fireEvent} from "@testing-library/react";
import Action from '../components/commons/Chart/Action';
import { Field } from '../components/commons/styled';

class TestAction extends React.Component {
  state = {
    defaultArea: '',
    defaultDropdownSelected: '',
    filterSelected: 'all'
  }

  componentDidMount() {
    this.setState({
      defaultArea: Object.keys(this.props.data)[0],
      defaultDropdownSelected: Object.keys(this.props.data)[0]
    });
  }

  getDataDropdown = value => {
    this.setState({ defaultArea: value.id, defaultDropdownSelected: value.id });
  };

  filterOnClick = event => {
    this.setState({ filterSelected: event.target.value });
  };

  listArea = data => {
    const getAreaName = Object.keys(data);
    let newListArea = [];
    
    getAreaName.forEach(d => {
      newListArea.push({
        key: d,
        text: d
      });
    });
     return newListArea;
  };

  renderFilter = data => {
    let temp = {};
    for (let key in data) {
      if (key === this.state.defaultArea) {
        temp = data[key];
      }
    }

    const category = Object.keys(temp).map( i => {
      return {
        key: i,
        text: i
      }
    });
    category.unshift({key:'all', text: 'all'});
    if (Object.keys(data).length !== 0) {
      return (
        <Action
          radio={category}
          radioOnclick={this.filterOnClick}
          radioSelected={this.state.filterSelected}
          horizontal
        />
      );
    }
    return null;
  };

  renderDropdown = data => {
    const { defaultDropdownSelected } = this.state;

    if (Object.keys(data).length !== 0) {
      const newListArea = this.listArea(data);

      return (
        <Action
          dropdown={newListArea}
          dataOutput={this.getDataDropdown}
          dataDefault={defaultDropdownSelected}
        />
      );
    }
    return null;
  };

  render() {
    const { data, id, className } = this.props;
    return (
      <Field className={`chart-container ${className}`} id={id}>
        {this.renderDropdown(data)}
        {this.renderFilter(data)}
      </Field>
    );
  }
}

const data = {
  area1 : {
    '23:00' : 12,
    '22:00' : 30,
    '21:00' : 22,
    '20:00' : 12
  },
  area2 : {
    'mobil' : 100,
    'motor' : 40,
  }
}

describe(`ChartActionTesting`, ()=> {
  it(`Should render`, () => {
    const { asFragment } = render(
      <TestAction data={data} />
    )

    expect(asFragment()).toMatchSnapshot();
  })

  it(`Could click dropdown and filter`, () => {
    const { getAllByText } = render(
      <TestAction data={data} />
    )
    
    let event = getAllByText('area1')[0];
    fireEvent.click(event);
    fireEvent.click(getAllByText('area2')[0]);
    fireEvent.click(getAllByText("mobil")[0]);
    fireEvent.click(getAllByText("all")[0]);
    
  })
})