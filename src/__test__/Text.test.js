import React from 'react';
import Text from '../components/commons/Input/Text';
import { render, fireEvent} from "@testing-library/react";

const types = ["textarea", "number"];

describe(`Text testing`, () => {
  types.map((val, idx) => {
    it(`Always rendered correctly ${val} ${idx}`, () => {
      const { asFragment, getByLabelText } = render(
        <Text
          title={val}
          type={val}
          id={idx}
          placeholder={`placeholder ${val}`}
        />
      );
      
      expect(asFragment()).toMatchSnapshot();
      
      const input = getByLabelText(val);
      fireEvent.change(input, {target: {value: "Nurul"}});
      if (idx === 1) {
        expect(input.value).toBe("");
      } else {
        expect(input.value).toBe("Nurul");
      }
    })
  })
})