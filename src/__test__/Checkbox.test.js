import React from 'react';
import { render, fireEvent} from "@testing-library/react";
import Checkbox from '../components/commons/Input/Checkbox';

const types = ["check", "strip", "error"];

const choice = [
  {key: "run", text: "running to the sun"},
  {key: "play", text: "playing with a doll"},
  {key: "jeng", text: "jeng jeng jeng"},
]

describe(`Checkbox testing`, () => {
  types.map((val, idx) => {
    it(`Always rendered correctly ${val} ${idx}`, () => {
      const { asFragment, getByLabelText } = render(
        <Checkbox
          title="test"
          id="test"
          onChange={()=> {}}
          type={val}
          dataInput={choice}
          disabled={ idx === 0 }
          horizontal={ idx === 1 }
        />
      );
      
      expect(asFragment()).toMatchSnapshot();
      
      const input = getByLabelText(/run/i);
      fireEvent.change(input, {target: {checked: true}});
      
    })
  })
})