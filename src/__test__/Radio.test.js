import React from 'react';
import { render, fireEvent} from "@testing-library/react";
import Radiobutton from '../components/commons/Input/Radio';

const types = ["blue", "red"];

const choice = [
  {key: "run", text: "running to the sun"},
  {key: "play", text: "playing with a doll"},
  {key: "jeng", text: "jeng jeng jeng"},
]

describe(`Radio testing`, () => {
  types.map((val, idx) => {
    it(`Always rendered correctly ${val} ${idx}`, () => {
      const { asFragment, getByLabelText } = render(
        <Radiobutton
          title="test"
          id="test"
          onChange={()=> {}}
          type={val}
          dataInput={choice}
          disabled={ idx === 0 }
          horizontal={ idx === 1 }
        />
      );
      
      expect(asFragment()).toMatchSnapshot();
      
      const input = getByLabelText(/run/i);
      fireEvent.change(input, {target: {value: "jeng"}});
      
    })
  })
})