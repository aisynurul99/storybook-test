import React from "react";
import Input from '../components/commons/Input';
import { Form } from '../components/commons/Input/styled';
import Layout from '../components/commons/Layout';
import '@testing-library/jest-dom/extend-expect';
import { render, cleanup, fireEvent } from "@testing-library/react";



class TestInputs extends React.Component {
  
  state = {
    name: '',
    age: 20, // input group 3
    birth: '', // input group 3
    gender: '', // dropdown // input group 3
    email: '', // input group 2
    password: '', // input group 2
    desc: '', // textarea
    hobby: [
      {key: 'write', text: 'write'},
      {key: 'read', text: 'read'},
      {key: 'sing', text: 'sing song'}
    ], // checkbox
    single: '', // radio
  }

  handleValue = eventName => {
    return e => {
      if (eventName === 'single') {
        const key = e.target.name;
        const checked = e.target.value;
        this.setState({
          [key]: checked
        });
      } else if (eventName === 'hobby') {
        const key = e.target.name;
        const checked = e.target.checked;

        // this.setState({
        //   [key]: checked
        // });
        let newItems = this.state.hobby.map( i => {
          if (i.key === key) {
            return {
              key: i.key,
              text: i.text,
              checked: checked
            }
          } else {
            return {
              key: i.key,
              text: i.text,
              checked: i.checked,
            }
          }
        })

        this.setState({
          hobby: newItems
        })

        
      } else if (eventName === 'gender') {
        const key = e.name; // name refer to title
        const checked = e.id; // refer to key that choosed
        this.setState({
          [key]: checked
        });
      } else {
        this.setState({
          [eventName] : e.target.value
        });
      }
    }
  }
  

  render() {
    const dataGender = [
      {key: 'male', text: 'male'},
      {key: 'female', text: 'female'}
    ]

    const dataHobby = [
      {key: 'write', text: 'write'},
      {key: 'read', text: 'read'},
      {key: 'sing', text: 'sing song'}
    ]

    const dataSingle = [
      {key: 'yes', text: 'single'},
      {key: 'no', text: 'in relationship'}
    ]
    const { name, age, gender, email, password, birth, desc, hobby, single } = this.state;
    
    return (
      <div>
        <Form>
          <Layout>
            <Input
                id='name'
                type='text'
                title='Name'
                placeholder='Input name'
                className='className'
                onChange={this.handleValue('name')}
                value={name}
                errorMessage="error"
                >
            </Input>
          </Layout>
          <Layout type='grid'>
            <Input
              id='age'
              type='number'
              title='Age'
              placeholder='Input age'
              className='className'
              onChange={this.handleValue('age')}
              value={age}
              >
            </Input>
            <Input
              id='birth'
              type='date'
              title='Birth'
              placeholder='Input birth'
              className='className'
              onChange={this.handleValue('birth')}
              value={birth}
              >
            </Input>
            <Input
              id='gender'
              type='dropdown'
              title='Gender'
              className='className'
              onChange={this.handleValue('gender')}
              dataInput={dataGender}
              >
            </Input>
          </Layout>
          <Layout type='grid'>
            <Input
              id='email'
              type='email'
              title='Email'
              placeholder='Input email'
              className='className'
              onChange={this.handleValue('email')}
              value={email}
              >
            </Input>
            <Input
              id='password'
              type='password'
              title='Password'
              placeholder='Input password'
              className='className'
              onChange={this.handleValue('password')}
              value={password}
              >
            </Input>
          </Layout> 
          <Layout>
            <Input
              id='desc'
              type='textarea'
              title='Description'
              placeholder='Input description'
              className='className'
              onChange={this.handleValue('desc')}
              value={desc}
            >
            </Input>
            <Input
              id='hobby'
              title='Hobbies?'
              type='checkbox'
              onChange={this.handleValue('hobby')}
              dataInput={dataHobby}
              horizontal
            >
            </Input>
            <Input
              id='single'
              title='Are you single?'
              type='radio'
              typeRadio='red'
              onChange={this.handleValue('single')}
              dataInput={dataSingle}
              value= {this.state.single}
              horizontal
            >
            </Input>
            <button onClick={this.submit}>Submit</button>
          </Layout>
        </Form>
        <div> Tes {this.state.name}</div>
      </div>
    )
  }
}

describe(`Inputs`, () => {
  it(`Always render when define `, () => {
    const { asFragment } = render(<TestInputs />);
    expect(asFragment()).toMatchSnapshot();
  });

  it(`should can receive input`, () => {
    const {getByLabelText, getByText, getByTestId, getAllByTestId} = render(<TestInputs/>);
    
    const input = getByLabelText('Name');
    fireEvent.change(input, {target: {value: "Nurul"}});

  })

  

  it(`cek`, () => {
    let name = ""
    const getText = (e) => {
      name = e.target.value; 
    };
    const { getByLabelText } = render(
      <div>
        <Input
          id='name'
          type='text'
          title='Name'
          placeholder='Input name'
          className='className'
          onChange={getText}
          value={name}
          >
        </Input>
        <div data-testid="tes">{name}</div>
      </div>
     
    );
    const input = getByLabelText('Name');
    fireEvent.change(input, {target: {value: "Nurul"}});
    expect(input.value).toBe("Nurul");
  })

})
