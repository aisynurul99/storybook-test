import React from 'react';
import { render, fireEvent} from "@testing-library/react";
import Radiobutton from '../components/commons/Input/Radio';
import Dropdown from '../components/commons/Input/Dropdown';

const types = ["blue", "red"];

const choice = [
  {key: "run", text: "running to the sun"},
  {key: "play", text: "playing with a doll"},
  {key: "jeng", text: "jeng jeng jeng"},
]

describe(`Dropdown testing`, () => {
  types.map((val, idx) => {
    it(`Always rendered correctly ${val} ${idx}`, () => {
      const { asFragment, getAllByText } = render(
        <Dropdown
          title="test"
          id="test"
          onChange={()=> {}}
          type={val}
          dataInput={choice}
          disabled={ idx === 0 }
          horizontal={ idx === 1 }
        />
      );
      
      expect(asFragment()).toMatchSnapshot();
      
      const input = getAllByText(/run/i)[0];
      fireEvent.click(input)
      
      
    })
  })
})