import React from 'react';
import { render, fireEvent} from "@testing-library/react";
import Chart from '../components/commons/Chart';
import ExampleBar from '../components/example/Chart/ExampleBar';

const dataBar = {
  area1 : {
    '23:00' : 12,
    '22:00' : 30,
    '21:00' : 22,
    '20:00' : 12
  },
  area2 : {
    'mobil' : 100,
    'motor' : 40,
  }
}

const dataLine = {
  area1 : {
    '23:00' : 'flood',
    '22:00' : 'level1',
    '21:00' : 'level1',
    '20:00' : 'flood',
  },
  area2 : {
    '23:00' : 'save',
    '22:00' : 'level1',
    '21:00' : 'flood',
    '20:00' : 'flood',
  }
}

const dataBar2Series = {
  area1 : {
    '23:00' : {
      speed : 12,
      amount : 10, 
    },
    '22:00' : {
      speed : 30,
      amount : 10, 
    },
    '21:00' : {
      speed : 22,
      amount : 70, 
    },
    '20:00' : {
      speed : 12,
      amount : 10, 
    }
  },
  area2 : {
    '23:00' : {
      speed : 100,
      amount : 10, 
    },
    '22:00' : {
      speed : 40,
      amount : 10, 
    },
    '21:00' : {
      speed : 72,
      amount : 70, 
    },
    '20:00' : {
      speed : 12,
      amount : 10, 
    }
  }
}

const legend = ['speed', 'amount'];
const unit = ['kmph', 'unit'];

const types = ["bar-2-series", "bar", "line", "wrong"]
const yAxis = ['save', 'level1', 'flood']



describe(`Chart testing`, () => {
  types.map((val, idx) => {
    it(`Always rendered correctly ${val} ${idx}`, () => {
      const { asFragment, getByText } = render(
        // <ExampleBar/>
        <div></div>
      );
      
      expect(asFragment()).toMatchSnapshot();
      
      // const input = getByText(/area 1/i);
      // console.log(input);
      
      // fireEvent.change(input, {target: {checked: true}});
      
    })
  })
})