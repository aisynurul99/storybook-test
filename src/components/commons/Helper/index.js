export function pxToVw(size) {
  // const screenWidth = window.innerWidth;
  const screenWidth = 1440; // 1440 => zeplin design based on 1440 screen size
  let result = 0;
  if (screenWidth > 1024) {
    result = ((size * 100) / screenWidth).toFixed(2);
    return result + 'vw';
  } else {
    return size + 'px';
  }
}
