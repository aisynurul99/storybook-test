import React from 'react';
import { GridLayout} from './styled';
import { Field } from '../styled';


export default class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      type,
      className,
    } = this.props;
    

    
    let com;

    switch (type) {
      case 'grid': 
        com = (
          <GridLayout
            id={id}
            className={className}
          >
            {this.props.children}
          </GridLayout>
        )
        break;
      case 'radio':
        com = (
          <div></div>
        )
        break;
      case 'dropdown':
        com = (
          com = (
           <div/>
          )
        )
        break;
      default:
        com =(
          <Field>
            {this.props.children}
          </Field>
        );
        break;
    }

    
    return (com);
  }
}