import Styled from 'styled-components';

export const GridLayout =  Styled.div`
  
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 10%;
  
  
`;