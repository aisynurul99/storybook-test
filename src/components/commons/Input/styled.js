import Styled from 'styled-components';
import { pxToVw } from '../Helper/index';

// style untuk text
export const Form = Styled.div`
  padding: 0.34722vw 0;
  font-family: 'Dosis';
  label {
    display: block;
    font-size: ${pxToVw(16)};
    margin-bottom: ${pxToVw(10)};
    margin-top: ${pxToVw(10)};
    color: #d8d8ff;
  }
  
  textarea {
    width: 100%;
    min-height: ${pxToVw(100)};
    max-height: ${pxToVw(200)};
    padding-top: 10px;
    padding-bottom: 10px;
  }
  input[type='text'],
  input[type='password'],
  input[type='number'],
  input[type='email'],
  input[type='date'],
  select,
  textarea {
    color: #d8d8ff;
    transition-duration: 0.3s;
    border: 1px solid #d8d8ff;
    border-radius: 2px;
    padding-left: 0.9722vw;
    padding-right: 0.9722vw;
    background: transparent;
    height: 2.2223vw;
    /* width: 20.83332vw; */
    width: 100%;
    box-sizing: border-box;
    &::placeholder {
      color: #d8d8ff;
      font-size: ${pxToVw(12)};
    }

    &:-ms-input-placeholder {
      /* Internet Explorer 10-11 */
      color: #d8d8ff;
      font-size: ${pxToVw(12)};
    }

    &::-ms-input-placeholder {
      /* Microsoft Edge */
      color: #d8d8ff;
      font-size: ${pxToVw(12)};
    }
    &:focus {
      outline:none;
      box-shadow: 0 0 5px 1px rgba(166, 137, 244, 0.78);
      border: solid 1px #8a5eff;
    }
    &.error {
      color: #ff5883;
      border: 1px solid #ffabc0;
      box-shadow: 0 0 4px 0.5px #ff5883, inset 0 0 4px 0.5px #ff5883;

      &::placeholder {
        color: #ffabc0;
        font-size: ${pxToVw(12)};
      }

      &:-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        color: #ffabc0;
        font-size: ${pxToVw(12)};
      }

      &::-ms-input-placeholder {
        /* Microsoft Edge */
        color: #ffabc0;
        font-size: ${pxToVw(12)};
      }

      &:focus {
        color: white;
      }
    }
  }
  .error-message {
    color: #ff5883;
    display: block;
    font-size: ${pxToVw(12)};
    margin-top: 0.2778vw;
  }


// checkbox style
  line-height: 16px !important;

  div.checkbox-container {
    margin-bottom: 10px;
    .horizontal {
      display: flex;
      justify-content: space-around;
      flex-wrap: wrap;
    }
    .vertical {
      display: flex;
      flex-direction: column;
    }

  }
  .checkbox-item {
    padding: 5px;
  }
  .styled-checkbox {
    position: absolute;
    opacity: 0;

    & + label {
      position: relative;
      cursor: pointer;
      padding: 0;
      margin: unset;
      margin-left: 10px;
      color: #d8d8ff;
      width: max-content;
    }
    

    /* // Box. */
    & + label:before {
      content: '';
      margin-right: 10px;
      display: inline-block;
      vertical-align: text-top;
      width: 15px;
      height: 15px;
      /* background: white; */
      border: 1px solid;
    }

    /* // Box hover */
    &:hover + label:before {
      background: #56a6fc;
    }

    /* // Box focus */
    &:focus + label:before {
      box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.12);
    }

    /* // Box checked */
    &:checked + label:before {
      background: #56a6fc;
    }

    &.strip:checked + label {
      &:after {
        content: '';
        position: absolute;
        left: 2px;
        top: 7px;
        background: white;
        transform: none;
        width: 10px;
        height: 2px;
        box-shadow: none;
      }
    }

    &.error:checked + label {
      &:after {
        font-family: 'nodeflux' !important;
        content: '\\44';
        line-height: 1;
        position: absolute;
        left: 2.8px;
        top: 3.5px;
        font-size: ${pxToVw(13)};
        transform: translateY(-50%) scale(0.9);

        box-shadow: none;
      }
      &:before {
        background-color: #b11f1f;
        border: 1px solid white;
      }
    }

    /* // Disabled state label. */
    &:disabled + label {
      color: #b8b8b8;
      cursor: auto;
    }

    /* // Disabled box. */
    &:disabled + label:before {
      box-shadow: none;
      background: #ddd;
    }

    /* // Checkmark. Could be replaced with an image */
    &:checked + label:after {
      content: '';
      position: absolute;
      left: 3px;
      top: 7px;
      background: white;
      width: 2px;
      height: 2px;
      box-shadow: 2px 0 0 white, 4px 0 0 white, 4px -2px 0 white,
        4px -4px 0 white, 4px -6px 0 white, 4px -8px 0 white;
      transform: rotate(45deg) scale(0.9);
    }
  }

// radiobutton style
  .radio-container {
    display: block;
    position: relative;
    cursor: pointer;
    font-size: ${pxToVw(14)};
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    margin-bottom: 10px;
    
    input[type='radio']:disabled{
      cursor: auto;
      &~.label-radio {
        color: #b8b8b8;
        cursor: auto;
      }
    } 
    
    .radio-item {
      display: flex;
      align-items: center;
    }
    .label-radio {
      color: #d8d8ff;
      font-size: ${pxToVw(16)};
      position:relative;
      display:block;
      cursor: pointer;
      margin: 0;
      align-items: center;
    }
    
    input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      height: 0;
      width: 0;
      left: 0;
      top: 0;
      z-index: 1;
    }
    .checkmark {
      position: relative;
      transform: scale(0.65);
      height: 25px;
      width: 25px;
      background-color: #eee;
      border-radius: 50%;
      cursor: pointer;
    }
    &:hover input ~ .checkmark {
      background-color: #ccc;
    }
    input:checked ~ .checkmark {
      background-color: white;
    }
    .checkmark:after {
      content: '';
      position: absolute;
      display: none;
    }
    & input:checked ~ .checkmark:after {
      display: block;
    }
    & .checkmark.blue {
      &:after {
        top: 3px;
        left: 3px;
        width: 19px;
        height: 19px;
        border-radius: 50%;
        transform: scale(0.8);
        background-color: #2196f3;
      }
    }
    & .checkmark.red {
      &:after {
        top: 3px;
        left: 3px;
        width: 19px;
        height: 19px;
        border-radius: 50%;
        border-radius: 50%;
        background-color: red;
      }
    }
    .horizontal {
      display: flex;
      justify-content: space-around;
      flex-wrap: wrap;
    }
    .vertical {
      display: flex;
      flex-direction: column;
    }
  }
  
;`


export const Wrapper = Styled.div`
  position: relative;
  width: 100%;
  display: inline-block;
  button{
    display: block;
  }
  
`;

export const Button = Styled.button`
  position: relative;
  background-color: transparent;
  outline: none;
  padding: 10px;
  cursor: pointer;
  text-align: left;
  color: #d8d8ff;
  border: none;
  &.fluid {
    border: 1px solid;
    border-radius: 2px;
    width: 100%;
    display: flex !important;
    justify-content: space-between !important;
  }
  &.disabled{
    pointer-events: none;
  }

  i{
    font-size: ${pxToVw(9)};
    margin-left: 5px;
  }
  
`;
export const Popup = Styled.div`
  position: absolute;
  top: 100%;
  left: 0;
  background-color: #d8d8ff;
  overflow: hidden;
  border-radius: 7px;
  z-index: 2;
  &.fluid {
    width: 100%;
  }
  
  button{
    color: #5a49ae;
    &:hover{
      background-color: #ccccff;
    }
  }
`;