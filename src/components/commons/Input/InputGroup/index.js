import React from 'react';
import { Field } from '../../styled'

export default class InputGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputField: ''
    };
  }
  render() {
    return (
      <Field className='input-group'>
        {this.props.children}
      </Field> 
    );
  }
}