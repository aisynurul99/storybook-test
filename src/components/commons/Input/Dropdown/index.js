import React from 'react';
import { Wrapper, Button, Popup} from '../styled';

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: false,
      dataOutput: this.props.dataInput[0].key
    };
  }

  componentDidMount() {
    this.props.onChange(
      {
        id: this.props.dataInput[0].key,
        name: this.props.title,
      } 
    )
    ;
  }
  componentDidUpdate(prevProps) {
    if (prevProps.dataDefault !== this.props.dataDefault) {
      const setDefault = this.props.dataInput[0];
      this.setState({
        dataOutput: setDefault
      });
    }
  }

  clickSelectHanlder = event => {
    this.togglePupup();
    this.setState({ dataOutput: event.target.id });
    this.props.onChange(event.target);
  };
  togglePupup = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed });
  };
  render() {
    const { isCollapsed } = this.state;
    const { dataInput, id, className, title, fluid } = this.props;

    return (
      <Wrapper id={id} className={className}>
        <label>{title}</label>
        {dataInput.map(i => {
          if (dataInput.length > 1) {
            if (i.key === this.state.dataOutput) {
              return (
                <Button
                  className={`area-selected ${fluid? 'fluid':''}`}
                  onClick={this.togglePupup}
                  id={i.key}
                  key={i.key}
                  name={title}
                  
                >
                  {i.text}{' '}
                  <i className={`icon-caret-${isCollapsed ? 'up' : 'down'}  ${fluid? 'fluid':''}`} />
                </Button>
              );
            } else {
              return null;
            }
          } else {
            return (
              <Button name={title} className={`area-selected disabled  ${fluid? 'fluid':''}`} id={i.key} key={i.key}>
                {i.text}
              </Button>
            );
          }
        })}
        {isCollapsed && (
          <Popup className={fluid? 'fluid':''}>
            {dataInput.map(i => {
              if (i.key !== this.state.dataOutput) {
                return (
                  <Button
                    name={title}
                    key={i.key}
                    onClick={this.clickSelectHanlder}
                    id={i.key}
                    className= {fluid? 'fluid':''}
                  >
                    {i.text}
                  </Button>
                );
              } else {
                return null;
              }
            })}
          </Popup>
        )}
      </Wrapper>
    );
  }
}