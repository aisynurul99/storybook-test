import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import { Field } from '../../styled';

const CHECK = 'check';
const STRIP = 'strip';
const ERROR = 'error';

export default class Checkbox extends React.Component {
  

  static propTypes = {
    type: PropTypes.oneOf([CHECK, STRIP, ERROR]),
    disabled: PropTypes.bool
  };

  static defaultProps = {
    type: CHECK,
    disabled: false
  };

  render() {
    const { dataInput, type, onChange, id, className, title, horizontal} = this.props;
    
    let item = dataInput.map( item => (
      <Field data-tip={item.tooltip} key={item.key} className={`checkbox checkbox-item ${className}`} >
        <ReactTooltip />
        <input
            className={`styled-checkbox ${type}`}
            type="checkbox"
            value={item.key}
            id={item.key}
            onChange={onChange}
            checked={item.checked}
            disabled={item.disabled}
            name={item.key}
        />
        <label htmlFor={item.key}> {item.text}</label>
      </Field>
    ))
    return (
      <Field id={id} className={`checkbox-container ${className}`} >
        {title && <label>{  title }</label>}
        <div className={horizontal? 'horizontal':'vertical'}>
          {item}
        </div>
        
        
      </Field>
      
    );
  }
}