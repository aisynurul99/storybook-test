import React from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import {Field} from '../../styled';


const BLUE = 'blue';
const RED = 'red';

export default class Radiobutton extends React.Component {
  static propTypes = {
    type: PropTypes.oneOf([BLUE, RED]),
    disabled: PropTypes.bool
  };

  static defaultProps = {
    type: BLUE,
    disabled: false
  };

  render() {
    const { dataInput, onChange, type, id, className, title, value, horizontal} = this.props;
     
    let item = dataInput.map( item => (
      <Field data-tip={item.tooltip} key={item.key} className={`radio radio-item ${className}`} >
        { item.tooltip && <ReactTooltip />}
        <input
            type="radio"
            value={item.key}
            id={item.key}
            onChange={onChange}
            checked={item.key === value}
            disabled={item.disabled}
            name={id}
        />
        <label htmlFor={item.key} className={'checkmark ' + type}> </label>
        <label htmlFor={item.key} className='label-radio'>{ item.text } </label>
      </Field>
    ))
    return (
      <Field id={id} className={`radio-container ${className}`}>
        {title && <label>{ title }</label>}
        <div className={horizontal? 'horizontal': 'vertical'}>
          {item}
        </div>
      </Field>
    );
  }
}