import React from 'react';
import {Field} from '../../styled';

export default class Text extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      type,
      title,
      placeholder,
      errorMessage,
      className,
      value
    } = this.props;
    const err = typeof errorMessage !== 'undefined' ? 'error' : '';
   
    let com;

    if (type === 'textarea')
      com = (
        <textarea
          id={`input-${id}`}
          placeholder={placeholder}
          className={err}
          value={value}
          onChange={this.props.onChange}
        />
      );
    else
      com = (
        <input
          id={`input-${id}`}
          type={type}
          placeholder={placeholder}
          className={err}
          value={value}
          onChange={this.props.onChange}
        />
      );

    return (
      <Field id={id} className={className}>
        <label htmlFor={`input-${id}`}>{title}</label>
        {com}
        <span >{errorMessage}</span>
      </Field>
    );
  }
}