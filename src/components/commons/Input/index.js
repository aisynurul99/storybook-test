import React from 'react';
import Text from './Text';
import Checkbox from './Checkbox';
import Radiobutton from './Radio';
import Dropdown from './Dropdown';

export default class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      type,
      title,
      placeholder,
      errorMessage,
      className,
      value,
      typeCheckbox, // check, strip, error
      typeRadio, // blue, red
      dataInput, // minimal 2 object dlm array[{key:'', text: '' }]
      onChange,
      horizontal,
      fluid
    } = this.props;
    
    
    
    let com;

    switch (type) {
      case 'checkbox':
        com = (
          <Checkbox
            id={id}
            className={className}
            type={typeCheckbox? typeCheckbox : 'check'}
            onChange={onChange}
            dataInput={dataInput}
            horizontal={horizontal}
            title={title}
            />
        )
        break;
      case 'radio':
        com = (
          <Radiobutton
          id={id} 
          className={className}
          type={typeRadio? typeRadio : 'blue'}
          onChange={onChange}
          title={title}
          dataInput= {dataInput}
          value={value}
          horizontal={horizontal}
          />
        )
        break;
      case 'dropdown':
        com = (
          com = (
            <Dropdown
              id={id}
              title={title}
              className={className}
              dataInput={dataInput}
              onChange={onChange}
              fluid={fluid}
            />
          )
        )
        break;
      default:
        com =(
          <Text
            id={id} 
            type={type}
            title={title}
            placeholder={placeholder}
            className={className}
            value={value}
            onChange={onChange}
            errorMessage={errorMessage}
          />
        );
        break;
    }

    
    return (com);
  }
}