import React from 'react';
import PropTypes from 'prop-types';
import { HOST, LOCAL_PORT } from 'config';
import styled, { css } from 'styled-components';

export default class Button extends React.Component {
  static propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    isLoading: PropTypes.bool,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    className: PropTypes.string,
    style: PropTypes.object,
    type: PropTypes.oneOf(['primary', 'purple', 'purple-cancel'])
  };

  static defaultProps = {
    type: 'primary'
  };

  render() {
    const { text, isLoading, onClick, id, className, style, type } = this.props;
    return (
      <ButtonStyle
        isLoading={!isLoading}
        disabled={isLoading}
        bigWidth
        onClick={onClick}
        id={id}
        className={className}
        style={style}
        type={type}
      >
        {isLoading === true ? (
          <Icon src={HOST + ':' + LOCAL_PORT + '/images/loader.gif'} />
        ) : (
          text
        )}
      </ButtonStyle>
    );
  }
}

const ButtonStyle = styled.button`
  font-family: "Dosis";
  font-size: 14px;
  letter-spacing: 1px;
  line-height: 16px;
  text-decoration: none;
  padding: 7px 15px 9px;
  height: 32px;
  border-radius: 61px;
  min-width: 100px;
  outline: none;
  display: flex;
  font-weight: 300;
  justify-content: center;
  align-items: center;
  margin-bottom: 0;
  touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  border: 1px solid transparent;
  white-space: nowrap;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  & > :not(:first-child) {
    margin-left: 8px;
  }
  & > * {
    display: flex !important;
  }
  &[disabled] {
    background: #ececec;
  }


  ${props =>
    props.isLoading &&
    css`
      border: none;
      color: #ffffff;
      font-weight: bold;
      color: white;
    `}

  /*background color option*/
  ${props =>
    props.type === 'primary' &&
    css`
      background: linear-gradient(to left, #ff75f1 0%, #ff0b98 100%);
      &:hover {
        background: linear-gradient(to left, #ff75f1 0%, #ff0b98 100%);
        box-shadow: inset 0 0 11px 0 rgba(0, 0, 0, 0.3);
      }
    `}
  ${props =>
    props.type === 'purple' &&
    css`
      box-shadow: 0 2px 20px 2px rgba(198, 86, 255, 0.35);
      background-image: linear-gradient(to right, #7242d9, #8b0acd);
    `}
  ${props =>
    props.type === 'purple-cancel' &&
    css`
      background-color: transparent;
      border-style: solid;
      border-width: 1px;
      border: solid 1px #7242d9;
    `}

  ${props =>
    props.circle &&
    css`
      width: 40px;
      padding: 0px;
      border-radius: 50%;
    `}
  ${props =>
    props.sm &&
    css`
      height: 30px;
      border-radius: 100px;
      padding: 6px 15px 8px;
    `}  
  ${props =>
    props.second &&
    css`
      height: 30px;
    `}
  ${props =>
    props.bigWidth &&
    css`
      width: 127px;
    `}
`;

const Icon = styled.img`
  width: 25px;
  height: 25px;
  margin-top: -5px;
`;
