import React from 'react';
import PropTypes from 'prop-types';
import { HOST, LOCAL_PORT, TYPE_RESTREAMER } from '../../../../config/config';
import styled from 'styled-components';

const Wrapper = styled.div`
  position: relative;
  width: ${props => props.width? props.width : "100%"};
  height: ${props => props.height? props.height  : "100%" };
  background-color: black;
  &:before {
    display: block;
    content: "";
    width: 100%;
    padding-top: ${props => props.ratio? (1/props.ratio)*100 + "% !important"  : "100%" };
  }
  > img {
    background-color: green;
    position: absolute;
    width: 100%;
    height: 100%;
    overflow: hidden;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
  }
`;
const Cctv = styled.img`
  width: ${props => props.ratio? "100%": props.width };
  height: ${props => props.ratio? (1/props.ratio)*100 + "% !important"  : props.height };
`;
export default class RestreamerVideo extends React.Component {
  static propTypes = {
    source: PropTypes.string,
    ratio: PropTypes.number,
    width: PropTypes.string,
    height: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      view: (
        <Wrapper
          width={this.props.width} 
          height={this.props.height}
          ratio={this.props.ratio} >
          <Cctv
            src={HOST + ':' + LOCAL_PORT + '/images/ph-nocctv.png'}
            className="cctv"
          />
        </Wrapper>
      )
    };
  }
  componentDidMount() {
    this.initialCreateChoosingPlayer();
    this.interval = setInterval(
      () => this.initialCreateChoosingPlayer(),
      60000
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps != this.props) {
      this.initialCreateChoosingPlayer();
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  changeImage = event => {
    event.target.src = `${HOST}:${LOCAL_PORT}/images/ph-streamerror.png`;
  };
  initialCreateChoosingPlayer = () => {
    let { source, type } = this.props;
    let viewNoSource = (
      <Wrapper
        width={this.props.width}
        height={this.props.height}
        ratio={this.props.ratio} >
        <Cctv 
          src={HOST + ':' + LOCAL_PORT + '/images/ph-nocctv.png'} />
      </Wrapper>
      
    );
    let viewCase;
    let r = Math.random()
      .toString(36)
      .substring(7);
    source = source === null || source === '#' ? source : `${source}?ref=${r}`;
    switch (TYPE_RESTREAMER) {
      default:
        viewCase = <Wrapper
          width={this.props.width} 
          height={this.props.height} 
          ratio={this.props.ratio}   
        >
          <Cctv 
          
          onError={this.changeImage}
          src={source} />
        </Wrapper> ;
    }
    this.setState({ view: source ? viewCase : viewNoSource });
  };
  render() {
    let { view } = this.state;
    return view;
  }
}
