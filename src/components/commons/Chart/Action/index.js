import React from 'react';
import Input from '../../Input';
import { Form } from '../../Input/styled';
import { Field } from '../../styled';

const Action = props => {
  const {
    dropdown,
    radio,
    radioSelected,
    radioOnclick,
    dataOutput,
    dataDefault,
    horizontal
  } = props;

 
  return (
    <Field>
      { dropdown &&
        <Form>
          <Input
            type='dropdown'
            dataInput={dropdown}
            onChange={dataOutput}
            value={dataDefault}
          />
        </Form> }
      { radio &&
        <Form>
          <Input 
            type='radio'
            id='filter'
            typeRadio='red'
            dataInput={radio}
            onChange={radioOnclick}
            value={radioSelected}
            horizontal={horizontal}
          />
        </Form> }
    </Field>
  );


}

export default Action;
