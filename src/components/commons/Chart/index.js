import React, { Component } from 'react';
import Action from './Action';
import Graph from './Graph';
import PropTypes from 'prop-types';
import { Field } from '../styled';
import moment from 'moment';

class Chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultArea: '',
      defaultDropdownSelected: '',
      filterSelected: 'all'
    }
  }

  componentDidMount() {
    this.setState({
      defaultArea: Object.keys(this.props.data)[0],
      defaultDropdownSelected: Object.keys(this.props.data)[0]
    });
  }

  getDataDropdown = value => {
    this.setState({ defaultArea: value.id, defaultDropdownSelected: value.id });
  };

  listArea = data => {
    const getAreaName = Object.keys(data);
    let newListArea = [];
    
    getAreaName.forEach(d => {
      newListArea.push({
        key: d,
        text: d
      });
    });
     return newListArea;
  };

  filterOnClick = event => {
    
    this.setState({ filterSelected: event.target.value });
  };

  renderFilter = data => {
    let temp = {};
    for (let key in data) {
      if (key === this.state.defaultArea) {
        temp = data[key];
      }
    }

    const category = Object.keys(temp).map( i => {
      return {
        key: i,
        text: i
      }
    });
    category.unshift({key:'all', text: 'all'});
    if (Object.keys(data).length !== 0) {
      return (
        <Action
          radio={category}
          radioOnclick={this.filterOnClick}
          radioSelected={this.state.filterSelected}
          horizontal
        />
      );
    }
    return null;
  };

  renderDropdown = data => {
    const { defaultDropdownSelected } = this.state;

    if (Object.keys(data).length !== 0) {
      const newListArea = this.listArea(data);

      return (
        <Action
          dropdown={newListArea}
          dataOutput={this.getDataDropdown}
          dataDefault={defaultDropdownSelected}
        />
      );
    }
    return null;
  };

  reorderData = data => {
    let newData = [];
    let hours = [];

    data.forEach(x => {

      let formatMoment;
      let time;

      if (this.props.chartType === 'bar-2-series') {
        formatMoment = x.xAxis.split(' ').length === 1 ? 'HH' : 'DD MMM';
        time = moment(x.xAxis, formatMoment);
      } else {
        formatMoment = x.type.split(' ').length === 1 ? 'HH' : 'DD MMM';
        time = moment(x.type, formatMoment);
      }

      hours.push(time);
    });

    hours.sort((a, b) => a - b);
    if (this.props.chartType === 'bar-2-series') {
      hours.forEach(x => {
        data.forEach(y => {
          const time = y.xAxis;
          const datee = x._i; // ambil format moment.
          if (datee === time) {
            newData.push({
              xAxis: y.xAxis,
              yAxis1: y.yAxis1,
              yAxis2: y.yAxis2
            });
          }
        });
      });
    } else {
      hours.forEach(x => {
        data.forEach(y => {
          const time = y.type;
          const datee = x._i; // ambil format moment.
          if (datee === time) {
            newData.push({
              type: y.type,
              total: y.total
            });
          }
        });
      });
    }
    

    return newData;
  };

  renderChart = value => {
    let dataFormatted = [];
    const { defaultArea } = this.state; //set as key
    const dataArea = value[defaultArea];
    const { legend } = this.props;

    if (legend) {
      for (let key in dataArea) {
        dataFormatted.push({
          xAxis: key,
          yAxis1: dataArea[key][legend[0]],
          yAxis2: dataArea[key][legend[1]],

        });
      }
    } else {
      for (let key in dataArea) {
        dataFormatted.push({
          type: key,
          total: dataArea[key]
        });
      }
    }

    

    if (dataArea) {
      
      if (isNaN(parseInt(Object.keys(dataArea)[0]))) {
        if (this.state.filterSelected !== 'all') {
          
          value = dataFormatted.filter(i => {
            return i.type === this.state.filterSelected;
          });
        } else {
          value = dataFormatted;
        }
      } else {
        value = this.reorderData(dataFormatted);
      }

      return <Graph chartType={this.props.chartType} dataHistory={value} legend={this.props.legend} unit={this.props.unit}  category={this.props.category} yAxis={this.props.yAxis} />;
    } else {
      return <div className="text text-xl">Data Not Available</div>;
    }
  };

  render() {
    const { data, filter, dropdown, id, className } = this.props;
    return (
      <Field className={`chart-container ${className}`} id={id}>
        {dropdown && this.renderDropdown(data)}
        {filter && this.renderFilter(data)}
        {this.renderChart(data)}
      </Field>
    );
  }

}



export default Chart;
Chart.propTypes = {
  chartType: PropTypes.string.isRequired,
  filter: PropTypes.bool
}