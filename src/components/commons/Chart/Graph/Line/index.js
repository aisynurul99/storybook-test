import React from 'react';
import ReactEcharts from 'echarts-for-react';

const Line = ({ data, yAxis }) => {
  const dataX = data.map(i => i.type);
  const dataY = data.map(i => i.total);

  let tempY = [];
  let obj = {};
  let indexing = [];

  yAxis.forEach((d, idx) => {
    obj[d] = idx;
    indexing.push(idx);
  });

  dataY.forEach(d => {
    Object.keys(obj).forEach(n => {
      if (d === n) {
        tempY.push(obj[n]);
      }
    });
  });

  // Safe, Level 1, Level 2, Level 3, Flood
  //[0, 1, 2, 3, 4]

  const colors = ['#6ef7c8', '#f1b9ff', '#ffffff'];

  let rotate = 0;
  let fontSize = 12;

  if (window.innerWidth <= 1440) {
    rotate = 50;
    fontSize = 10;
  } else if (window.innerWidth <= 1700) {
    rotate = 45;
    fontSize = 11;
  }

  const option = {
    tooltip: {
      trigger: 'axis',
      formatter: function(params) {
        const param = params[0].value;
        const name = params[0].name;
        let result = [];
        Object.keys(obj).forEach(d => {
          if (param ===  obj[d]) {
            result = d;
          }
        });
        return name + ' : ' + result;
      }
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: dataX,
      axisLabel: {
        textStyle: {
          color: '#d8d8ff'
        }
      },
      axisLine: {
        show: false
      },
      offset: 15
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: function(value) {
          let result = [];
          Object.keys(obj).forEach(n => {
            if (value === obj[n]) {
              result = n;
            }
          });
          return result;
        },
        textStyle: {
          color: '#d8d8ff'
        },
        rotate: rotate,
        fontSize: fontSize,
        color: colors[2],
        shadowColor: colors[2],
        shadowBlur: 7
      },
      splitLine: {
        lineStyle: {
          type: 'dashed',
          color: '#6b69b9'
        }
      },
      axisLine: {
        show: false
      },
      axisTick: {
        show: false
      }
    },
    series: [
      {
        name: '最低气温',
        type: 'line',
        smooth: true,
        lineStyle: {
          color: colors[0],
          shadowColor: colors[0],
          shadowBlur: 7
        },
        itemStyle: {
          color: colors[0],
          borderColor: colors[0]
        },
        data: tempY
      }
    ]
  };

  return <ReactEcharts option={option} yAxis={yAxis} />;
};

export default Line;
