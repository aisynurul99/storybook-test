import React from 'react';
import PropTypes from 'prop-types';
import ReactEcharts from 'echarts-for-react';

const Bar2Series = ({ data, direction, legend, unit }) => {
  const xAxisData = data.map(i => i.xAxis);
  const seriesData1 = data.map(i => i.yAxis1);
  const seriesData2 = data.map(i => i.yAxis2);

  let option = {};

  // ====Start Responsive Bar Width ===== //
  let barItemWidth = 12;
  let fontSize = 12;
  if (window.innerWidth <= 1366) {
    barItemWidth = 8;
    fontSize = 10;
  }
  // ====End Responsive Bar Width ===== //

  const borderRadius =
    direction === 'horizontal' ? [0, 15, 15, 0] : [15, 15, 0, 0];

  option = {
    tooltip: {
      trigger: 'axis',
      position: 'inside',
      formatter: function (params) {
        
        let output = '';
        for (let i = 0; i < params.length; i++) {
          output += '<b>' + params[i].seriesName + ': </b>' + params[i].value + ' ' + unit[i] + ' <br/>'
        }
        return output
      }
    },
    legend: {
      data: legend,
      textStyle: {
        color: '#d8d8ff',
      },
      itemWidth: 12,
      borderRadius: 0,
      inactiveColor: '#6b69b9',
    },
    calculable: true,
    series: [
      {
        type: 'bar',
        barWidth: barItemWidth,
        name: legend[0],
        itemStyle: {
          normal: {
            color: '#ff75a7',
            barBorderRadius: borderRadius,
            shadowColor: 'rgba(255, 117, 167, 0.5)',
            shadowBlur: 10,
            shadowOffsetX: '6px',
            shadowOffsetY: '1px',
          }
        },
        data: seriesData1
      },
      {
        type: 'bar',
        barWidth: barItemWidth,
        name: legend[1],
        itemStyle: {
          normal: {
            color: '#febb29',
            barBorderRadius: borderRadius,
            shadowColor: 'rgba(255, 117, 167, 0.5)',
            shadowBlur: 10,
            shadowOffsetX: '6px',
            shadowOffsetY: '1px',
          }
        },
        data: seriesData2
      }
    ]
  };

  switch (direction) {
    case 'horizontal': {
      option['yAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['xAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }

    case 'vertical': {
      option['xAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['yAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }

    default: {
      option['xAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['yAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }
  }
  return (
    <ReactEcharts
      option={option}
      style={{ height: '300px', width: '100%', marginBottom: '20px' }}
    />
  );
};

export default Bar2Series;
Bar2Series.propTypes = {
  dataHistory: PropTypes.array
};
