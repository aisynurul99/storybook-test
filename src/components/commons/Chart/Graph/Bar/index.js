import React from 'react';
import PropTypes from 'prop-types';
import ReactEcharts from 'echarts-for-react';
import moment from 'moment';

const Bar = ({ dataHistoryBar, category, direction }) => {

  let xAxisData = dataHistoryBar.map(i => i.type);
  let seriesData = dataHistoryBar.map(i => i.total);
  
  if (category === 'timestamp') {
    let hoursData = [];
    dataHistoryBar.forEach(data => {
      const hour = moment(data.timestamp, 'YYYY-MM-DD HH:mm:ss').format(
        'HH:mm'
      );
      if (hoursData.indexOf(hour) !== -1) {
        seriesData[hoursData.indexOf(hour)] += parseInt(data.total);
      } else {
        hoursData.push(hour);
        if (parseFloat(data.total) === data.total) {
          seriesData.push(data.total);
        } else {
          seriesData.push(parseInt(data.total));
        }
      }
    });
    xAxisData = hoursData;
  } else if (category === 'date') {
    let dateData = [];

    dataHistoryBar.forEach(data => {
      const day = moment(data.date, 'YYYY-MM-DD').format('DD');

      if (dateData.indexOf(day) !== -1) {
        seriesData[dateData.indexOf(day)] += parseInt(data.total);
      } else {
        dateData.push(day);
        seriesData.push(parseInt(data.total));
      }
    });
    xAxisData = dateData;
  }


  let option = {};

  // ====Start Responsive Bar Width ===== //
  let barItemWidth = 12;
  let fontSize = 12;
  if (window.innerWidth <= 1366) {
    barItemWidth = 8;
    fontSize = 10;
  }
  // ====End Responsive Bar Width ===== //

  const borderRadius =
    direction === 'horizontal' ? [0, 15, 15, 0] : [15, 15, 0, 0];

  option = {
    tooltip: {
      trigger: 'item',
      position: 'inside'
    },
    legend: {
      data: ['Neutral']
    },
    calculable: true,
    series: [
      {
        type: 'bar',
        barWidth: barItemWidth,
        itemStyle: {
          normal: {
            color: '#7d6dfc',
            barBorderRadius: borderRadius
          }
        },
        data: seriesData
      }
    ]
  };

  switch (direction) {
    case 'horizontal': {
      option['yAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['xAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }

    case 'vertical': {
      option['xAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['yAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }

    default: {
      option['xAxis'] = [
        {
          type: 'category',
          show: true,
          data: xAxisData,
          axisLine: {
            show: false,
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            showMaxLabel: true
          }
        }
      ];
      option['yAxis'] = [
        {
          type: 'value',
          show: true,
          axisLabel: {
            fontSize: fontSize,
            formatter: function (value) {
              let result = 0;
              if (value < 1000) {
                return value;
              } else {
                result = value / 1000;
                return result + ' K';
              }
            }
          },
          axisLine: {
            lineStyle: {
              color: '#d8d8ff'
            }
          },
          axisTick: {
            show: false
          },
          splitLine: {
            lineStyle: {
              type: 'dashed',
              color: '#6b69b9'
            }
          }
        }
      ];
      break;
    }
  }

  return (
    <ReactEcharts
      option={option}
      style={{ height: '300px', width: '100%', marginBottom: '20px' }}
    />
  );
};

export default Bar;
Bar.propTypes = {
  dataHistory: PropTypes.array
};
