import React from 'react';
import PropTypes from 'prop-types';
import Bar from './Bar';
import Line from './Line';
// import HeatMap from './HeatMap';
import Bar2Series from './Bar2Series';

const Graph = props => {
  const {
    chartType,
    dataHistory,
    category,
    direction,
    threshold,
    yAxis,
    legend,
    unit
  } = props;
  switch (chartType) {
    case 'bar-2-series': {
      /**
       * example data
       * [
       *  {
       *    xAxis: 'A',
       *    yAxis1: 100,
       *    yAxis2: 50
       *  },
       *  {
       *    xAxis: 'B',
       *    yAxis1: 100,
       *    yAxis2: 50
       *  }
       * ]
       * 
       */
      return (
        <Bar2Series
          data={dataHistory}
          direction={direction}
          legend={legend}
          unit={unit}
        />
      );
    }
    case 'bar': {
      /**
       * example data
       *
       * [
       *    {
       *        type: 'A',
       *        total: 100,
       *    },
       * {
       *        type: 'B',
       *        total: 200
       *    }
       * ]
       */
      return (
        <Bar
          dataHistoryBar={dataHistory}
          category={category}
          direction={direction}
        />
      );
    }

    case 'line': {
      return (
        <Line
          dataHistoryLine={dataHistory}
          category={category}
          threshold={threshold}
          data={dataHistory}
          yAxis={yAxis}
        />
      );
    }

    // case 'hitmap': {
    //   return <HeatMap data={dataHistory} />;
    // }

    default:
      break;
  }
};

export default Graph;
Graph.propTypes = {
  chartType: PropTypes.oneOf([
    'line',
    'multi-line',
    'bar',
    'bar-2-series',
    'pie',
    'bar-time',
    'line-pkl',
    'line-water-level',
    'hitmap'
  ])
};
