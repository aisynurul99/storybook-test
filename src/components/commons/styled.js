import Styled from 'styled-components';

// div default
export const Field = Styled.div`
  position: relative;
  color: white;

  &.chart-container {
    .radio-container  .horizontal {
      display: flex;
      justify-content: center;
      flex-wrap: wrap;
      .radio-item {
        margin-left: 10px;
        margin-right: 10px;
      }
    }
  }
`;

