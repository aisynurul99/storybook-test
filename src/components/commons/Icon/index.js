import React from 'react';

const getChild = (data, fill, stroke) => {
  console.log('getchild', typeof data, data);
  if (!Array.isArray(data)) {
    let dataProps = data.props;
    let child = typeof dataProps.children === 'object' || Array.isArray(dataProps.children) ? getChild(dataProps.children, fill, stroke) : null;
    let comp;
    if (child) {
      comp = React.createElement(data.type, dataProps, child);
    } else {
      comp  = React.createElement(data.type, dataProps);
    }
    return comp;
  }
  let comps = data.map( (i, key) => {
    let iProps = i.props;
    let tempFill = iProps.fill && iProps.fill.startsWith("#") ? fill:iProps.fill;
    let tempStroke = iProps.stroke && iProps.stroke.startsWith("#") ? stroke:iProps.stroke;
    let props = {
      key
    };
    Object.keys(iProps).map( p => {
      if (p === 'fill') {
        return props[p]=tempFill;
      } else if (p === 'stroke') {
        return props[p]=tempStroke;
      }
      return props[p]=iProps[p];
    })
    console.log(props);
    let child = typeof iProps.children === 'object' || Array.isArray(iProps.children) ? getChild(iProps.children, fill, stroke) : null;
    let comp;
    if (child) {
      comp = React.createElement(i.type, props, child);
    } else {
      comp  = React.createElement(i.type, props);
    }
    return comp;
  })

  return comps
}

const getSvg = (data, fill, stroke, width, height, viewBox, id, className) => {
  let dataProps = data.props;
  let props = {
    width: width? width:dataProps.width,
    height: height? height:dataProps.height,
    viewBox: viewBox? viewBox:dataProps.viewBox,
    id: id,
    className: className
  }
  let child = getChild(dataProps.children, fill, stroke);
  let comp = React.createElement(data.type, props, child );
  console.log(comp);
  return comp;
  
}

const Icon = ({
  fill='#D8D8FF',
  stroke= '#D8D8FF',
  width = "12px",
  height = width,
  className = "",
  viewBox = "0 0 25 25",
  id,
  data
}) => {
  console.log(data);
  let comp = getSvg(data, fill, stroke, width, height, viewBox, id, className )
  return comp
}

export default Icon;