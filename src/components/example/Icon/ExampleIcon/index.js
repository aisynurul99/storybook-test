import React from 'react';
import BaseExample from '../../BaseExample';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Icon from '../../../commons/Icon';
import {
  PEOPLE_DWELLING,
  CCTV,
  CROUD_BEHAV,
  CROUD_DESTI,
  FR,
  LPR,
  PEOPLE_COUNTING,
  PEOPLE_INTRUSION,
  TRAFFIC_LIGHT,
  TRAJECTORY_FLOW,
  VEHICLE_DWELLING,
  VEHICLE_INTRUSION
} from '../../../../constant/icon'

export default class ExampleIcon extends React.Component {
  constructor(){
    super();
    this.state = {
      valueText: '',
      isTextImportCopied: false,
      isTextComponentCopied: false,
      isTextDataCopied: false

    }
    
  }
  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
 
  
  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'paths',
          'detail' : 'list of attribute d of path svg',
          'is required': 'true',
          'data type': 'list of string',
        },
        {
          'props' : 'style',
          'detail' : 'style for svg',
          'data type': 'object',
        },
        {
          'props' : 'width',
          'detail' : 'width for svg, default: 100%',
          'data type': 'string',
        },
        {
          'props' : 'viewBook',
          'detail' : 'viewBook for svg, default: 0 0 16 16',
          'data type': 'string',
        }
      ]
    };

    const textData = 
`// salah satu contoh di constant/icon

<svg width="25px" height="25px" viewBox="0 0 25 25" version="1.1" xmlns="http://www.w3.org/2000/svg">
  <title>icon/tab/cctv</title>
  <desc>Created with Sketch.</desc>
  <g id="icon/tab/cctv" stroke="none" strokeWidth="1" 
  fill="none" fillRule="evenodd">
    <rect id="Rectangle" x="0" y="0" width="25" height="25"></
    rect>
    <g id="cctv-(1)" transform="translate(4.000000, 6.000000)" 
    fill="#D8D8FF" fillRule="nonzero">
      <path d="M0,0 L0,3.71428571 L16.7142857,3.71428571 
        L16.7142857,0 L0,0 Z M13.6538086,2.43795981 L12.6744559,
        2.43795981 L12.6744559,1.2763259 L13.6538086,1.2763259 
        L13.6538086,2.43795981 Z M15.4003209,2.43795981 
        L14.4209682,2.43795981 L14.4209682,1.2763259 L15.4003209,
        1.2763259 L15.4003209,2.43795981 Z" id="Shape"></path>
      <path d="M8.35714286,6.5 C6.82109596,6.5 5.57142857,
        7.5413973 5.57142857,8.82144598 C5.57142857,10.1014598 
        6.82109596,11.1428571 8.35714286,11.1428571 C9.89318975,
        11.1428571 11.1428571,10.1014598 11.1428571,8.82144598 
        C11.1428571,7.54136249 9.89318975,6.5 8.35714286,6.5 Z 
        M8.35714286,10.2142857 C7.33313648,10.2142857 6.5,
        9.58945756 6.5,8.82144418 C6.5,8.05339958 7.33313648,
        7.42857143 8.35714286,7.42857143 C9.38114923,7.42857143 
        10.2142857,8.05339958 10.2142857,8.82144418 C10.2142857,
        9.58945756 9.38114923,10.2142857 8.35714286,10.2142857 
        Z" id="Shape"></path>
      <path d="M0.928571429,4.64285714 L0.928571429,5.3984511 
        C0.928571429,7.42890928 1.70127492,9.33781023 3.10435048,
        10.773557 C4.50742603,12.2093038 6.37288889,13 8.35714286,
        13 C10.3413968,13 12.2068597,12.2093038 13.6099352,
        10.773557 C15.0130108,9.33784402 15.7857143,7.42894306 
        15.7857143,5.39848489 L15.7857143,4.64285714 L0.928571429,
        4.64285714 Z M8.35714286,12.3721852 C6.309082,12.3721852 
        4.64285714,10.7059431 4.64285714,8.65789944 C4.64285714,
        6.60981741 6.309082,4.94361373 8.35714286,4.94361373 
        C10.4052037,4.94361373 12.0714286,6.60985582 12.0714286,
        8.65793786 C12.0714286,10.7060199 10.4052037,12.3721852 
        8.35714286,12.3721852 Z" id="Shape"></path>
    </g>
</g>
</svg> 
`;

    const textImport = 
`import {
  CCTV,
  PEOPLE_DWELLING
} from '../../../../constant/icon'` ;


    const textComponent =
`<Icon fill="blue" width="50px" data={CCTV} />`

    const description = 'Icon is a composition of 1 individual components – <Icon/>, but need path attribute.If there are new icon svg from UI team, we just need to add the necessary tag like below '

    const { isTextDataCopied, isTextComponentCopied, isTextImportCopied} = this.state;
    return (
      <BaseExample title='Icon' detailProps={detailProps} description={description}>
        <div className='box' >
          <Icon fill="blue" width="50px" data={CCTV} />
          <Icon fill="white" width="50px" data={PEOPLE_DWELLING} />
          <Icon fill="red" width="50px" data={CROUD_BEHAV} />
          <Icon width="50px" data={CROUD_DESTI} />
          <Icon fill="red" stroke="red" width="50px" data={FR} />
          <Icon width="50px" data={LPR} />
          <Icon width="50px" data={PEOPLE_COUNTING} />
          <Icon width="50px" data={TRAFFIC_LIGHT} />
          <Icon width="50px" data={TRAJECTORY_FLOW} />
          <Icon width="50px" data={PEOPLE_INTRUSION} />
          <Icon width="50px" data={VEHICLE_DWELLING} />
          <Icon width="50px" data={VEHICLE_INTRUSION} />
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the component and icon needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>

        <h4>2. Data - constant icon</h4>
        <p>Import the component and icon needed</p>
        <div className='code'>
          <pre>  
            { textData }
          </pre>
          <div className={`copied ${isTextDataCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={isTextDataCopied} 
            onCopy={
              this.copyClicked('isTextDataCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}