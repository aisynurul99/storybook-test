import React, { Component } from 'react';
import { Example } from '../styled';
import Table from './Table';

export default class BaseExample extends Component {
  render() {
    const { title, description, detailProps } = this.props
    return (
      <Example>
        <h1>{ title }</h1>
        <h3>Description</h3>
        <div>{description}</div>
        <h3>Example</h3>
        {this.props.children}
        {
          detailProps &&
          <div>
            <h3>{detailProps ? 'Detail Props' : ''}</h3>
            <Table header={detailProps.header} data={detailProps.data} />
          </div>
        }
        
      </Example>
    );
  }
}