import React from 'react';

export default class Table extends React.Component {
  render (){
    const { header, data } = this.props;

    let com = data.map( (i, idx) => {
      return (
        <tr key={idx}>
          {
            header.map( (j, idxx) => {
              return (
                <td key={idxx}>{ i[j]}</td>
              );
            })
          }
        </tr>
      )
    })

    return (
      <table>
        <thead>
          <tr>
            {
              header.map( (i, idx) => {
                return(
                  <th key={idx}>{ i }</th>
                )
              })
            }
          </tr>
        </thead>
        <tbody>
          {com} 
        </tbody>
        
      </table>
    )

    
  }
}