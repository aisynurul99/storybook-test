import React from 'react';
import BaseExample from '../../BaseExample';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import RestreamerVideo from '../../../commons/RestreamerVideo';

export default class ExampleRadio extends React.Component {
  constructor() {
    super();
    this.state = {
      isTextImportCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
      tes: 'text2'
    }
  }

  handleInputChange = (event) => {
    const key = event.target.name;
    const checked = event.target.value;
    this.setState({
      [key]: checked
    });
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName]: true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }


  render() {
    const detailProps = {
      header: ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props': 'ratio',
          'detail': 'resolution for image',
          'is required': 'true, if no width ',
          'data type': 'number'
        },
        {
          'props': 'width',
          'detail': 'width for image',
          'is required': 'true, if no ratio',
          'data type': 'string',
        },
        {
          'props': 'height',
          'detail': 'height for image',
          'data type': 'string',
        }
      ]
    };

    const textImport =
      `import RestreamerVideo from 'commons/RestreamerVideo';` ;

  

    const textDataInput =
      `const src1 = "http://www.pinkslipinspiration.com/newsite/
wp-content/uploads/2014/06/16-9-dummy-image2.jpg"
      `;

    const textComponent =
      `<RestreamerVideo  ratio={16/9} source={src1} />
<RestreamerVideo  ratio={3/2} source={src1} />
<RestreamerVideo  width="200px" source={src1} />`;

    const description = 'Restreamer Video is an individual components – <RestreamerVideo />.'

    const { isTextComponentCopied, isTextImportCopied, isTextDataInputCopied } = this.state;

    const src1 = "http://www.pinkslipinspiration.com/newsite/wp-content/uploads/2014/06/16-9-dummy-image2.jpg"

    return (
      <BaseExample title='Restreamer Video' detailProps={detailProps} description={description}>
        <h2>16:9</h2>
        <div className="box">
         <RestreamerVideo  ratio={16/9} source={src1} />
        </div>
        <h2>3:2</h2>
        <div className="box">
         <RestreamerVideo  ratio={3/2} source={src1} />
        </div>
        <h2>200px*200px</h2>
        <div className="box">
         <RestreamerVideo  width="200px" source={src1} />
        </div>
          

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            {textImport}
          </pre>
          <div className={`copied ${isTextImportCopied ? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard
            text={textImport}
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>

        <h4>2. Data</h4>
        <p>Data for checkbox item. attribute checked refer for value of state text1, and text 2</p>
        <div className='code'>
          <pre>
            {textDataInput}
          </pre>
          <div className={`copied ${isTextDataInputCopied ? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard
            text={textDataInput}
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>

        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>
            {textComponent}
          </pre>
          <div className={`copied ${isTextComponentCopied ? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard
            text={textComponent}
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>

      </BaseExample>
    );
  }
}