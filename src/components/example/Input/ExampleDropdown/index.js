import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import { CopyToClipboard } from 'react-copy-to-clipboard';

export default class ExampleDropdown extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
    }
  }
  handleInputChange = (event) => {
    const key = event.name; // name refer to title
    const checked = event.id; // refer to key that choosed
    this.setState({
      [key]: checked
    });
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
 
  
  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'type',
          'detail' : 'type of Input, must dropdown',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'onChange',
          'detail' : 'function for handle item choosed',
          'data type': 'function',
          'is required': 'true',
        },
        {
          'props' : 'dataInput',
          'detail' : 'data for each item radio, an object must have at least key, and text. Available property: key, text, disabled, and tooltip',
          'data type': 'Array of object',
          'is required': 'true'
        },
        {
          'props' : 'title',
          'detail' : 'for name of input dropdown, and label',
          'data type': 'string',
          'is required': 'true',
        },
        {
          'props' : 'fluid',
          'detail' : 'for styling, fluid mean width 100% and border',
          'data type': 'string'
        }
      ]
    };

    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';` ;

    const textFunction =
`handleInputChange = (event) => {
  const key = event.name; // name refer to title
  const checked = event.id; // refer to key that choosed
  this.setState({
    [key]: checked
  });
};`;

  const textDataInput = 
`const dataInput = [
  // place default value in the first index
  {key: 'text1', text: 'text 1'},
  {key: 'text2', text: 'text 2'},
  {key: 'text3', text: 'text 3'}
]`;

    const textComponent =
`<Form>
  <Input
    title='tes' // title for name and label
    type='dropdown'
    className='my-input'
    onChange={this.handleInputChange}
    dataInput={dataInput}
    fluid
  ></Input> 
</Form>`;

    const description = 'Dropdown is a composition of 2 individual components – <Form/> and <Input /> with type dropdown.'

    const { isTextComponentCopied, isTextFunctionCopied, isTextImportCopied, isTextDataInputCopied} = this.state;

    const dataInput = [
      {key: 'text1', text: 'text 1'},
      {key: 'text2', text: 'text 2'},
      {key: 'text3', text: 'text 3'}
    ]
    return (
      <BaseExample title='Dropdown | Selectbox' detailProps={detailProps} description={description}>
        <div className='box'>
          <Form>
            <Input
              title='tes' 
              type='dropdown'
              className='my-input'
              onChange={this.handleInputChange}
              dataInput={dataInput}
              fluid
            ></Input> 
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. input dropdown
          component just return object with name and id as keys. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Data</h4>
        <p>Data for dropdown item</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>4. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}