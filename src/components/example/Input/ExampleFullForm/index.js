import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Layout from '../../../commons/Layout';

export default class ExampleFullForm extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false,
      name: '',
      age: 20, // input group 3
      birth: '', // input group 3
      gender: '', // dropdown // input group 3
      email: '', // input group 2
      password: '', // input group 2
      desc: '', // textarea
      hobby: [
        {key: 'write', text: 'write'},
        {key: 'read', text: 'read'},
        {key: 'sing', text: 'sing song'}
      ], // checkbox
      single: '', // radio
      hasilSubmit: []
    }
  }

  handleValue = eventName => {
    return e => {
      if (eventName === 'single') {
        const key = e.target.name;
        const checked = e.target.value;
        this.setState({
          [key]: checked
        });
      } else if (eventName === 'hobby') {
        const key = e.target.name;
        const checked = e.target.checked;

        // this.setState({
        //   [key]: checked
        // });
        let newItems = this.state.hobby.map( i => {
          if (i.key === key) {
            return {
              key: i.key,
              text: i.text,
              checked: checked
            }
          } else {
            return {
              key: i.key,
              text: i.text,
              checked: i.checked,
            }
          }
        })

        this.setState({
          hobby: newItems
        })

        
      } else if (eventName === 'gender') {
        const key = e.name; // name refer to title
        const checked = e.id; // refer to key that choosed
        this.setState({
          [key]: checked
        });
      } else {
        this.setState({
          [eventName] : e.target.value
        });
      }
    }
  }

  submit = () =>{
    const { 
      name, age, gender, birth, desc, email, password, hobby, single
    } = this.state;

    let  x = [
      { name: name }, { age: age }, { gender: gender }, { birth: birth },
      { desc: desc }, { email: email}, { password: password }, 
      { single: single }, 
      { hobby: hobby.filter(i => i.checked)}
    ];  

    console.log('submit', x);
    
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }

  render() {
    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';
import Layout from 'commons/Layout';` ;

    const textFunction =
`handleValue = eventName => {
  return e => {
    if (eventName === 'single') {
      const key = e.target.name;
      const checked = e.target.value;
      this.setState({
        [key]: checked
      });
    } else if (eventName === 'hobby') {
      const key = e.target.name;
      const checked = e.target.checked;

      // this.setState({
      //   [key]: checked
      // });
      let newItems = this.state.hobby.map( i => {
        if (i.key === key) {
          return {
            key: i.key,
            text: i.text,
            checked: checked
          }
        } else {
          return {
            key: i.key,
            text: i.text,
            checked: i.checked,
          }
        }
      })

      this.setState({
        hobby: newItems
      })

      
    } else if (eventName === 'gender') {
      const key = e.name; // name refer to title
      const checked = e.id; // refer to key that choosed
      this.setState({
        [key]: checked
      });
    } else {
      this.setState({
        [eventName] : e.target.value
      });
    }
  }
}`

    const textComponent =
`<Form>
<Layout>
  <Input
      id='name'
      type='text'
      title='Name'
      placeholder='Input name'
      className='className'
      onChange={this.handleValue('name')}
      value={nama}
      >
  </Input>
</Layout>
<Layout type='grid'>
  <Input
    id='age'
    type='number'
    title='Age'
    placeholder='Input age'
    className='className'
    onChange={this.handleValue('age')}
    value={age}
    >
  </Input>
  <Input
    id='birth'
    type='date'
    title='Birth'
    placeholder='Input birth'
    className='className'
    onChange={this.handleValue('birth')}
    value={birth}
    >
  </Input>
  <Input
    id='gender'
    type='dropdown'
    title='Gender'
    className='className'
    onChange={this.handleValue('gender')}
    dataInput={dataGender}
    >
  </Input>
</Layout>
<Layout type='grid'>
  <Input
    id='email'
    type='email'
    title='Email'
    placeholder='Input email'
    className='className'
    onChange={this.handleValue('email')}
    value={email}
    >
  </Input>
  <Input
    id='password'
    type='password'
    title='Password'
    placeholder='Input password'
    className='className'
    onChange={this.handleValue('password')}
    value={password}
    >
  </Input>
</Layout> 
<Layout>
  <Input
    id='desc'
    type='textarea'
    title='Description'
    placeholder='Input description'
    className='className'
    onChange={this.handleValue('desc')}
    value={desc}
  >
  </Input>
  <Input
    id='hobby'
    title='Hobbies?'
    type='checkbox'
    onChange={this.handleValue('hobby')}
    dataInput={dataHobby}
    horizontal
  >
  </Input>
  <Input
    id='single'
    title='Are you single?'
    type='radio'
    typeRadio='red'
    onChange={this.handleValue('single')}
    dataInput={dataSingle}
    value= {this.state.single}
    horizontal
  >
  </Input>
  <button onClick={this.submit}>Submit</button>
</Layout>
</Form>`

    const description = 'This is example of full form'
    
    const dataGender = [
      {key: 'male', text: 'male'},
      {key: 'female', text: 'female'}
    ]

    const dataHobby = [
      {key: 'write', text: 'write'},
      {key: 'read', text: 'read'},
      {key: 'sing', text: 'sing song'}
    ]

    const dataSingle = [
      {key: 'yes', text: 'single'},
      {key: 'no', text: 'in relationship'}
    ]

    const { nama, age, email, password, birth, desc, isTextComponentCopied, isTextFunctionCopied, isTextImportCopied} = this.state;
    return (
      <BaseExample title='Example Form' description={description}>
        <div className='box'>
          <Form>
            <Layout>
              <Input
                  id='name'
                  type='text'
                  title='Name'
                  placeholder='Input name'
                  className='className'
                  onChange={this.handleValue('name')}
                  value={nama}
                  >
              </Input>
            </Layout>
            <Layout type='grid'>
              <Input
                id='age'
                type='number'
                title='Age'
                placeholder='Input age'
                className='className'
                onChange={this.handleValue('age')}
                value={age}
                >
              </Input>
              <Input
                id='birth'
                type='date'
                title='Birth'
                placeholder='Input birth'
                className='className'
                onChange={this.handleValue('birth')}
                value={birth}
                >
              </Input>
              <Input
                id='gender'
                type='dropdown'
                title='Gender'
                className='className'
                onChange={this.handleValue('gender')}
                dataInput={dataGender}
                >
              </Input>
            </Layout>
            <Layout type='grid'>
              <Input
                id='email'
                type='email'
                title='Email'
                placeholder='Input email'
                className='className'
                onChange={this.handleValue('email')}
                value={email}
                >
              </Input>
              <Input
                id='password'
                type='password'
                title='Password'
                placeholder='Input password'
                className='className'
                onChange={this.handleValue('password')}
                value={password}
                >
              </Input>
            </Layout> 
            <Layout>
              <Input
                id='desc'
                type='textarea'
                title='Description'
                placeholder='Input description'
                className='className'
                onChange={this.handleValue('desc')}
                value={desc}
              >
              </Input>
              <Input
                id='hobby'
                title='Hobbies?'
                type='checkbox'
                onChange={this.handleValue('hobby')}
                dataInput={dataHobby}
                horizontal
              >
              </Input>
              <Input
                id='single'
                title='Are you single?'
                type='radio'
                typeRadio='red'
                onChange={this.handleValue('single')}
                dataInput={dataSingle}
                value= {this.state.single}
                horizontal
              >
              </Input>
              <button onClick={this.submit}>Submit</button>
            </Layout>
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={this.copyClicked('isTextComponentCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}