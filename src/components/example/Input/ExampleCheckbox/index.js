import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class ExampleCheckbox extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
      text2: true,
      text1: true,
    }
  }

  handleInputChange = (event) => {
    const key = event.target.name;
    const checked = event.target.checked;

    this.setState({
      [key]: checked
    });
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
 
  
  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'type',
          'detail' : 'type of Input, must checkbox',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'typeCheckbox',
          'detail' : 'type of checkbox, default: check. Available typeCheckbox: check | strip | error',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'onChange',
          'detail' : 'function for handle item checked',
          'data type': 'function',
          'is required': 'true',
        },
        {
          'props' : 'dataInput',
          'detail' : 'data for each item checkbox, ane object must have at least key, and text. Available property: key, text, disabled, tooltip, checked ',
          'data type': 'Array of object',
          'is required': 'true'
        },
        {
          'props' : 'horizontal',
          'detail' : 'for styling, if props horizontal doesnt exist, then the layout be vertical',
          'data type': 'boolean',
        }
      ]
    };

    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';` ;

    const textFunction =
`handleInputChange = (event) => {
  const key = event.target.name;
  const checked = event.target.checked;

  this.setState({
    [key]: checked
  });
}`;

  const textDataInput = 
`const dataInput = [
  {key: 'text1', text: 'text 1', checked: this.state.text1, 
    disabled: true, tooltip: 'tooltip for disable'},
  {key: 'text2', text: 'text 2', checked: this.state.text2},
  {key: 'text3', text: 'text 3'}
]`;

    const textComponent =
`<Form>
  <Input
    type='checkbox'
    className='my-input'
    onChange={this.handleInputChange}
    dataInput={dataInput}
  ></Input> 
</Form>`;

    const description = 'Checkbox is a composition of 2 individual components – <Form/> and <Input /> with type checkbox.'

    const { text1, text2, isTextComponentCopied, isTextFunctionCopied, isTextImportCopied, isTextDataInputCopied} = this.state;

    const dataInput = [
      {key: 'text1', text: 'text 1', checked: text1, disabled: true, tooltip: 'tooltip for disable'},
      {key: 'text2', text: 'text 2', checked: text2},
      {key: 'text3', text: 'text 3'}
    ]
    return (
      <BaseExample title='Checkbox' detailProps={detailProps} description={description}>
        <div className='box'>
          <Form>
            <Input
              type='checkbox'
              className='my-input'
              onChange={this.handleInputChange}
              dataInput={dataInput}
            ></Input> 
            <br/>
            getText: { this.state.text1 }
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Data</h4>
        <p>Data for checkbox item. attribute checked refer for value of state text1, and text 2</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>4. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}