import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import { CopyToClipboard } from 'react-copy-to-clipboard';

export default class ExampleRadio extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
      tes: 'text2'
    }
  }

  handleInputChange = (event) => {
    const key = event.target.name;
    const checked = event.target.value;
    this.setState({
      [key]: checked
    });
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
  
  
  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component and as name for input radio',
          'data type': 'string'
        },
        {
          'props' : 'type',
          'detail' : 'type of Input, must radio',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'typeRadio',
          'detail' : 'type of radio, default: blue. Available typeRadio: blue | red',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'onChange',
          'detail' : 'function for handle item choosed',
          'data type': 'function',
          'is required': 'true',
        },
        {
          'props' : 'dataInput',
          'detail' : 'data for each item radio, an object must have at least key, and text. Available property: key, text, disabled, and tooltip',
          'data type': 'Array of object',
          'is required': 'true'
        },
        {
          'props' : 'value',
          'detail' : 'add value default. use state for dynamic. note that the value must be equal to one of key on data input',
          'data type': 'string',
        },
        {
          'props' : 'title',
          'detail' : 'for name of input radio',
          'data type': 'string',
          'is required': 'true',
        },
        {
          'props' : 'horizontal',
          'detail' : 'for styling, if props horizontal doesnt exist, then the layout be vertical',
          'data type': 'boolean',
        }
      ]
    };

    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';` ;

    const textFunction =
`handleInputChange = (event) => {
  const key = event.target.name;
  const checked = event.target.value;

  this.setState({
    [key]: checked
  });
}`;

  const textDataInput = 
`const dataInput = [
  {key: 'text1', text: 'text 1', disabled: true, 
    tooltip: 'tooltip for disable'},
  {key: 'text2', text: 'text 2'},
  {key: 'text3', text: 'text 3'}
]`;

    const textComponent =
`<Form>
  <Input
    id='tes' // id for name
    title='tes'
    type='radio'
    className='my-input'
    onChange={this.handleInputChange}
    dataInput={dataInput}
    value={this.state.tes}
  ></Input> 
</Form>`;

    const description = 'Radio is a composition of 2 individual components – <Form/> and <Input /> with type radio.'

    const { isTextComponentCopied, isTextFunctionCopied, isTextImportCopied, isTextDataInputCopied} = this.state;

    const dataInput = [
      {key: 'text1', text: 'text 1', disabled: true, tooltip: 'tooltip for disable'},
      {key: 'text2', text: 'text 2'},
      {key: 'text3', text: 'text 3'}
    ]
    return (
      <BaseExample title='Radio' detailProps={detailProps} description={description}>
        <div className='box'>
          <Form>
            <Input
              title='Pilih salah satu' 
              id='tes' // id for name
              type='radio'
              className='my-input'
              onChange={this.handleInputChange}
              dataInput={dataInput}
              value={this.state.tes} // sesuai dengan name
            ></Input> 
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Data</h4>
        <p>Data for checkbox item. attribute checked refer for value of state text1, and text 2</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>4. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}