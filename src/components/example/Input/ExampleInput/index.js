import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class ExampleInput extends React.Component {
  constructor(){
    super();
    this.state = {
      valueText: '',
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false

    }
  }
  getText = (e) => {
    this.setState(
      {
        valueText : e.target.value
      }
    )
  };

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
 
  
  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'type',
          'detail' : 'type of Input, like text | textarea | number | email | password, etc',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'title',
          'detail' : 'label text for the input',
          'data type': 'string',
        },
        {
          'props' : 'placeholder',
          'detail' : 'placeholder for the input',
          'data type': 'string',
        },
        {
          'props' : 'errorMessage',
          'detail' : 'message if user input has error',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'value',
          'detail' : 'value for the input, must dynamic like refer to state',
          'data type': 'string',
        },
        {
          'props' : 'onChange',
          'detail' : 'function for handle input text, like to get the value',
          'data type': 'function',
          'is required': 'true',
        },
        {
          'props' : 'disabled',
          'detail' : 'if disabled to input text',
          'data type': 'boolean',
        },
      ]
    };

    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';` ;

    const textFunction =
`getText = (e) => {
  this.setState(
    {
      valueText : e.target.value
    }
  )
};`

    const textComponent =
`<Form>
  <Input
    type='text'
    placeholder= 'Placeholder'
    className='tes class'
    onChange={this.getText} 
    title='Contoh input judul'
  ></Input> 
  <br/>
  getText: { this.state.valueText }
</Form>`

    const description = 'Text Input is a composition of 2 individual components – <Form/> and <Input />.'

    const { valueText, isTextComponentCopied, isTextFunctionCopied, isTextImportCopied} = this.state;
    return (
      <BaseExample title='Text Input' detailProps={detailProps} description={description}>
        <div className='box'>
          <Form>
            <Input
              type='text'
              placeholder= 'Placeholder'
              className='my-input'
              onChange={this.getText} 
              title='Title for input'
            ></Input> 
            <br/>
            getText: { valueText }
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}