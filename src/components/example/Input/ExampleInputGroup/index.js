import React from 'react';
import Input from '../../../commons/Input';
import { Form } from '../../../commons/Input/styled'
import BaseExample from '../../BaseExample';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import Layout from '../../../commons/Layout';

export default class ExampleInputGroup extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextFunctionCopied: false,
      isTextComponentCopied: false,
      name: '',
      age: 20,
      gender: 'female',
      email: '',
      password: '',
      birth: '',

    }
  }
  handleValue = eventName => {
    return e => {
      this.setState({
        [eventName] : e.target.value
      });
    }
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }
  
  render() {
    const detailProps = {
      header : ['component', 'props', 'data type', 'is required', 'detail'],
      data: [
        {
          'component': '<Layout/>',
          'props' : 'type',
          'detail' : 'type for Layout, default: block. Available type: grid',
          'data type': 'string'
        },
        {
          'component': '<Input/>, <Layout/>',
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'component': '<Input/>',
          'props' : 'type',
          'detail' : 'type of Input, like text | textarea | number | email | password, etc',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'component': '<Input/>',
          'props' : 'title',
          'detail' : 'label text for the input',
          'data type': 'string',
        },
        {
          'component': '<Input/>',
          'props' : 'placeholder',
          'detail' : 'placeholder for the input',
          'data type': 'string',
        },
        {
          'component': '<Input/>',
          'props' : 'errorMessage',
          'detail' : 'message if user input has error',
          'data type': 'string',
        },
        {
          'component': '<Input/>, <Layout/>',
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'component': '<Input/>',
          'props' : 'value',
          'detail' : 'value for the input, must dynamic like refer to state',
          'data type': 'string',
        },
        {
          'component': '<Input/>',
          'props' : 'onChange',
          'detail' : 'function for handle input text, like to get the value',
          'data type': 'function',
          'is required': 'true',
        },
        {
          'component': '<Input/>',
          'props' : 'disabled',
          'detail' : 'if disabled to input text',
          'data type': 'boolean',
        },

      ]
    };

    const textImport = 
`import Input from 'commons/Input';
import { Form } from 'commons/Input/styled';
import Layout from 'commons/Layout';` ;

    const textFunction =
`handleValue = eventName => {
  return e => {
    this.setState({
      [eventName] : e.target.value
    });
  }
}`

    const textComponent =
`<Form>
  <Layout>
    <Input
        id='name'
        type='text'
        title='Name'
        placeholder='Input name'
        className='className'
        onChange={this.handleValue('name')}
        value={nama}
        >
    </Input>
  </Layout>
  <Layout type='grid'>
    <Input
      id='age'
      type='number'
      title='Age'
      placeholder='Input age'
      className='className'
      onChange={this.handleValue('age')}
      value={age}
      >
    </Input>
    <Input
      id='gender'
      type='text'
      title='Gender'
      placeholder='Input gender'
      className='className'
      onChange={this.handleValue('gender')}
      value={gender}
      >
    </Input>
  </Layout>
  <Layout type='grid'>
    <Input
      id='email'
      type='email'
      title='Email'
      placeholder='Input email'
      className='className'
      onChange={this.handleValue('email')}
      value={email}
      errorMessage='email format'
      >
    </Input>
    <Input
      id='password'
      type='password'
      title='Password'
      placeholder='Input password'
      className='className'
      onChange={this.handleValue('password')}
      value={password}
      >
    </Input>
    <Input
      id='birth'
      type='date'
      title='Birth'
      placeholder='Input birth'
      className='className'
      onChange={this.handleValue('birth')}
      value={birth}
      >
    </Input>
  </Layout> 
</Form>`

    const description = 'Text Input Group is a composition of 3 individual components – <Form/>, <Layout /> and <Input />. It use for group some input in one row.' 

    const { nama, age, gender, email, password, birth, isTextComponentCopied, isTextFunctionCopied, isTextImportCopied} = this.state;
    return (
      <BaseExample title='Text Input Group' detailProps={detailProps} description={description}>
        <div className='box'>
          <Form>
            <Layout>
              <Input
                  id='name'
                  type='text'
                  title='Name'
                  placeholder='Input name'
                  className='className'
                  onChange={this.handleValue('name')}
                  value={nama}
                  >
              </Input>
            </Layout>
            <Layout type='grid'>
              <Input
                id='age'
                type='number'
                title='Age'
                placeholder='Input age'
                className='className'
                onChange={this.handleValue('age')}
                value={age}
                >
              </Input>
              <Input
                id='gender'
                type='text'
                title='Gender'
                placeholder='Input gender'
                className='className'
                onChange={this.handleValue('gender')}
                value={gender}
                >
              </Input>
            </Layout>
            <Layout type='grid'>
              <Input
                id='email'
                type='email'
                title='Email'
                placeholder='Input email'
                className='className'
                onChange={this.handleValue('email')}
                value={email}
                >
              </Input>
              <Input
                id='password'
                type='password'
                title='Password'
                placeholder='Input password'
                className='className'
                onChange={this.handleValue('password')}
                value={password}
                >
              </Input>
              <Input
                id='birth'
                type='date'
                title='Birth'
                placeholder='Input birth'
                className='className'
                onChange={this.handleValue('birth')}
                value={birth}
                >
              </Input>
            </Layout> 
          </Form>
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>2. Function Handler</h4>
        <p>Need to create a function handler to handle user input. For example:</p>
        <div className='code'>
          <pre>
            { textFunction }
          </pre>
          <div className={`copied ${isTextFunctionCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textFunction} 
            onCopy={this.copyClicked('isTextFunctionCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={this.copyClicked('isTextComponentCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
        
      </BaseExample>
    );
  }
}