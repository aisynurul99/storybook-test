import React from 'react';
import BaseExample from '../../BaseExample';
import Chart from '../../../commons/Chart';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class ExampleLine extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
    }
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }

  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'chartType',
          'detail' : 'type of chart, must line',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'dropdown',
          'detail' : 'if have dropdown',
          'data type': 'boolean'
        },
        {
          'props' : 'filter',
          'detail' : 'if have filter',
          'data type': 'boolean',
        },
        {
          'props' : 'data',
          'detail' : 'data to be chart',
          'data type': 'object',
          'is required': 'true',
        },
        {
          'props' : 'yAxis',
          'detail' : 'data to be yAxis chart, must match with the values of data',
          'data type': 'Array of String',
          'is required': 'true',
        }
      ]
    };
    const textImport = 
`import Chart from 'commons/Chart';` ;

    
      const textDataInput = 
`const data= {
  area1 : {
    '23:00' : 'flood',
    '22:00' : 'level1',
    '21:00' : 'level1',
    '20:00' : 'flood',
  },
  area2 : {
    '23:00' : 'save',
    '22:00' : 'level1',
    '21:00' : 'flood',
    '20:00' : 'flood',
  }
}

const yAxis = ['save', 'level1', 'flood']`;
    
        const textComponent =
`<Chart dropdown data={data} yAxis={yAxis}  chartType='line' />`;
    
    const description = 'Line is a composition of 1 individual components – <Chart/> with type line'

    const { isTextComponentCopied, isTextImportCopied, isTextDataInputCopied} = this.state;

    const data= {
      area1 : {
        '23:00' : 'flood',
        '22:00' : 'level1',
        '21:00' : 'level1',
        '20:00' : 'flood',
      },
      area2 : {
        '23:00' : 'save',
        '22:00' : 'level1',
        '21:00' : 'flood',
        '20:00' : 'flood',
      }
    }

    const yAxis = ['save', 'level1', 'flood']


    
    return(
      <BaseExample title='Line' detailProps={detailProps} description={description}>
        <div className='box'>
          <Chart dropdown data={data} yAxis={yAxis}  chartType='line' />
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        
        <h4>2. Data</h4>
        <p>Data for chart. the structure data is strict, must similiar</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
      </BaseExample>

    );
  }
}