import React from 'react';
import BaseExample from '../../BaseExample';
import Chart from '../../../commons/Chart';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class ExampleBar2Series extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
    }
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }

  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'chartType',
          'detail' : 'type of chart, must bar-2-series',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'legend',
          'detail' : 'legend of chart, the value must same with the deepest',
          'data type': 'array of string',
          'is required': 'true',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'unit',
          'detail' : 'Unit is mean for unit for the data, match with legend example: legend is speed, unit is kmph',
          'data type': 'array of string',
          'is required': 'true',
        },
        {
          'props' : 'dropdown',
          'detail' : 'if have dropdown',
          'data type': 'boolean'
        },
        {
          'props' : 'filter',
          'detail' : 'if have filter',
          'data type': 'boolean',
        }
      ]
    };
    const textImport = 
`import Chart from 'commons/Chart';` ;

    
      const textDataInput = 
`const legend = ['speed', 'amount'];
const unit = ['kmph', 'unit'];
const data= {
  area1 : {
    '23:00' : {
      speed : 12,
      amount : 10, 
    },
    '22:00' : {
      speed : 30,
      amount : 10, 
    },
    '21:00' : {
      speed : 22,
      amount : 70, 
    },
    '20:00' : {
      speed : 12,
      amount : 10, 
    }
  },
  area2 : {
    '23:00' : {
      speed : 100,
      amount : 10, 
    },
    '22:00' : {
      speed : 40,
      amount : 10, 
    },
    '21:00' : {
      speed : 72,
      amount : 70, 
    },
    '20:00' : {
      speed : 12,
      amount : 10, 
    }
  }
}`;
    
        const textComponent =
`<Chart dropdown data={data} legend={legend} unit={unit}
 chartType='bar-2-series' />`;
    
    const description = 'Bar2Series is a composition of 1 individual components – <Chart/> with type bar-2-series.'

    const { isTextComponentCopied, isTextImportCopied, isTextDataInputCopied} = this.state;


    const legend = ['speed', 'amount'];
    const unit = ['kmph', 'unit'];
    const data= {
      area1 : {
        '23:00' : {
          speed : 12,
          amount : 10, 
        },
        '22:00' : {
          speed : 30,
          amount : 10, 
        },
        '21:00' : {
          speed : 22,
          amount : 70, 
        },
        '20:00' : {
          speed : 12,
          amount : 10, 
        },
        // '19:00' : {
        //   speed : 12,
        //   amount : 10, 
        // },
        // '18:00' : {
        //   speed : 30,
        //   amount : 10, 
        // },
        // '17:00' : {
        //   speed : 22,
        //   amount : 70, 
        // },
        // '16:00' : {
        //   speed : 12,
        //   amount : 10, 
        // },
        // '15:00' : {
        //   speed : 12,
        //   amount : 10, 
        // },
        // '14:00' : {
        //   speed : 30,
        //   amount : 10, 
        // },
        // '13:00' : {
        //   speed : 22,
        //   amount : 70, 
        // },
        // '12:00' : {
        //   speed : 12,
        //   amount : 10, 
        // },
        // '11:00' : {
        //   speed : 30,
        //   amount : 10, 
        // },
        // '10:00' : {
        //   speed : 22,
        //   amount : 70, 
        // },
        // '09:00' : {
        //   speed : 12,
        //   amount : 10, 
        // },
      },
      area2 : {
        '23:00' : {
          speed : 100,
          amount : 10, 
        },
        '22:00' : {
          speed : 40,
          amount : 10, 
        },
        '21:00' : {
          speed : 72,
          amount : 70, 
        },
        '20:00' : {
          speed : 12,
          amount : 10, 
        }
      }
    }

    
    return(
      <BaseExample title='Bar2Series' detailProps={detailProps} description={description}>
        <div className='box'>
          <Chart dropdown data={data} legend={legend} unit={unit} chartType='bar-2-series' />
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        
        <h4>2. Data</h4>
        <p>Data for chart. the structure data is strict, must similiar</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
      </BaseExample>

    );
  }
}