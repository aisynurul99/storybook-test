import React from 'react';
import BaseExample from '../../BaseExample';
import Chart from '../../../commons/Chart';
import {CopyToClipboard} from 'react-copy-to-clipboard';

export default class ExampleBar extends React.Component {
  constructor(){
    super();
    this.state = {
      isTextImportCopied: false,
      isTextComponentCopied: false,
      isTextDataInputCopied: false,
    }
  }

  copyClicked = eventName => {
    return () => {
      this.setState({
        [eventName] : true
      });
      setTimeout(() => {
        this.setState({ [eventName]: false });
      }, 1000);
    }
  }

  render() {
    const detailProps = {
      header : ['props', 'data type', 'is required', 'detail'],
      data: [
        {
          'props' : 'id',
          'detail' : 'id for the component',
          'data type': 'string'
        },
        {
          'props' : 'chartType',
          'detail' : 'type of chart, must bar',
          'is required': 'true',
          'data type': 'string',
        },
        {
          'props' : 'className',
          'detail' : 'add class name for component',
          'data type': 'string',
        },
        {
          'props' : 'dropdown',
          'detail' : 'if have dropdown',
          'data type': 'boolean'
        },
        {
          'props' : 'filter',
          'detail' : 'if have filter',
          'data type': 'boolean',
        },
        {
          'props' : 'data',
          'detail' : 'data to be chart',
          'data type': 'object',
          'is required': 'true',
        }
      ]
    };
    const textImport = 
`import Chart from 'commons/Chart';` ;

    
      const textDataInput = 
`const data= {
  area1 : {
    '23:00' : 12,
    '22:00' : 30,
    '21:00' : 22,
    '20:00' : 12
  },
  area2 : {
    'mobil' : 100,
    'motor' : 40,
    'orang' : 72,
    'truk' : 12
  }
}`;
    
        const textComponent =
`<Chart dropdown filter data={data} chartType='bar' />`;
    
    const description = 'Bar is a composition of 1 individual components – <Chart/> with type bar'

    const { isTextComponentCopied, isTextImportCopied, isTextDataInputCopied} = this.state;

    const data= {
      area1 : {
        '23:00' : 12,
        '22:00' : 30,
        '21:00' : 22,
        '20:00' : 12
      },
      area2 : {
        'mobil' : 100,
        'motor' : 40,
      }
    }

    
    return(
      <BaseExample title='Bar' detailProps={detailProps} description={description}>
        <div className='box'>
          <Chart dropdown filter data={data} chartType='bar' />
        </div>

        <h3>Method of Use</h3>
        <h4>1. Import</h4>
        <p>Import the both components needed</p>
        <div className='code'>
          <pre>
            { textImport }
          </pre>
          <div className={`copied ${isTextImportCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textImport} 
            onCopy={
              this.copyClicked('isTextImportCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        
        <h4>2. Data</h4>
        <p>Data for chart. the structure data is strict, must similiar</p>
        <div className='code'>
          <pre>
            { textDataInput }
          </pre>
          <div className={`copied ${isTextDataInputCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textDataInput} 
            onCopy={this.copyClicked('isTextDataInputCopied')}
          >
            <button>Copy</button>
          </CopyToClipboard>
        </div>
        
        <h4>3. Use of Component</h4>
        <div className='code'>
          <pre>  
            { textComponent }
          </pre>
          <div className={`copied ${isTextComponentCopied? '' : 'hidden'}`}>
            copied
          </div>
          <CopyToClipboard 
            text={textComponent} 
            onCopy={
              this.copyClicked('isTextComponentCopied')
            }
          >
            <button>Copy</button>
          </CopyToClipboard>  
        </div>
      </BaseExample>

    );
  }
}