import Styled from 'styled-components';

// div default
export const Example = Styled.div`
  position: relative;
  width: 97%;
  color: white;
  padding: 20px;
  background-image: linear-gradient(to bottom, #1e164c, #170f33);
  .box {
    width: 500px;
    padding: 10px;
    border-radius: 10px;
    
  }
  .code {
    background-color: white;
    color: black;
    padding: 10px;
    width: 500px;
    border-radius: 10px;
    padding-bottom: 50px;
    position: relative;
    button {
      position: absolute;
      display: block;
      cursor: pointer;
      float: right;
      right: 10px;
      bottom: 20px;
    }
    .copied {
      position: absolute;
      background-image: linear-gradient(to bottom, #1e164c, #170f33);
      color: white
      border-radius: 10px;
      padding: 5px;
      width: max-content;
      float: right;
      right: 10px;
      bottom: 50px;
      transition: opacity 0.5s;
      opacity: 1;
      &.hidden {
        opacity: 0;
        transition: opacity 0.5s;
      }
    }
  }
  
  table {
    width: 100%;
    
    th {
      background-image: linear-gradient(to bottom, #1e164c, #170f33);
      height: 30px;
    }
    
    td {
      font-size: 16px;
      height: 50px;
      padding-left: 5px;
      border-bottom: 1px solid white;
    }
  }
  
`;
