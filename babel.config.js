module.exports = {
  presets: ["@babel/preset-env", "@babel/preset-react"],
  plugins: [
    "babel-plugin-styled-components",
    "@babel/transform-runtime",
    "@babel/proposal-class-properties",
    "@babel/proposal-object-rest-spread"
  ]
};
