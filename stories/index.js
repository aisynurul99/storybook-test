import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from '@storybook/react/demo';
import '../src/styles/styles.css'

require('./input/InputText.story');
require('./Chart/Chart.story');
require('./Icon/Icon.story');
require('./RestreamerVideo/RestreamerVideo.story');
storiesOf('Button', module)
  .add('with text', () => (
    <Button>Hello Button</Button>
  ))
  .add('with emoji', () => (
    <Button><span role="img" aria-label="so cool">😀 😎 👍 💯</span></Button>
  ));   