import React from 'react';
import { storiesOf } from '@storybook/react';
import ExampleIcon from '../../src/components/example/Icon/ExampleIcon';

storiesOf('Icon', module)
  .add('Icon', () => 
    <ExampleIcon />
  )
  ;