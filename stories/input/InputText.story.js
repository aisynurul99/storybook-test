import React from 'react';
import { storiesOf } from '@storybook/react';

import ExampleInputGroup from '../../src/components/example/Input/ExampleInputGroup';
import ExampleCheckbox from '../../src/components/example/Input/ExampleCheckbox';
import ExampleDropdown from '../../src/components/example/Input/ExampleDropdown';
import ExampleRadio from '../../src/components/example/Input/ExampleRadio';
import ExampleInput from '../../src/components/example/Input/ExampleInput';
import ExampleFullForm from '../../src/components/example/Input/ExampleFullForm';

export let fromchild;

export function getText(e){
  console.log(e.target.value);
};

export function getValueDropdown(e) {
  console.log(e);
}

export const inputText = {
  id: '1',
  label: 'Test Task',
  type: 'text',
  className: 'test classs',
  onChange: getText
};

export const preSuffix = {
  prefix: 'prefix',
  suffix: 'suffix'
};

export const options = [
  {text:'tes 1', key: '1'},
  {text:'tes 2', key: '2'},
  {text:'tes 3', key: '3'}
]

storiesOf('Input', module)
  .add('Input Default', () => 
    <ExampleInput/>
  )
  .add('Input Group', () => 
    <ExampleInputGroup></ExampleInputGroup>
  )
  .add('Input Checkbox', () => 
  <div>
    <ExampleCheckbox></ExampleCheckbox>
  </div>
  )
  .add('Input Radio', () => 
    <ExampleRadio></ExampleRadio>
  )
  .add('Input Dropdown', () => 
    <ExampleDropdown></ExampleDropdown>
  )
  .add('Full Form', () => 
    <ExampleFullForm></ExampleFullForm>
  )
  ;