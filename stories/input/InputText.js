import React from 'react';
import Text from '../../src/components/Input/Text'
export default function InputText({ inputText: { id, label, type, onSubmit, className }}) {
  
  return (
    <Text label={label} id={id} type={type} onSubmit={onSubmit} className={className}></Text>
  );
}