import React from 'react';
import { storiesOf } from '@storybook/react';
import ExampleBar2Series from '../../src/components/example/Chart/ExampleBar2Series';
import ExampleBar from '../../src/components/example/Chart/ExampleBar';
import ExampleLine from '../../src/components/example/Chart/ExampleLine';


storiesOf('Chart', module)
  .add('Bar 2 Series', () => 
    <ExampleBar2Series />
  )
  .add('Bar', () => 
    <ExampleBar />
  )
  .add('Line', () => 
    <ExampleLine />
  )
  ;