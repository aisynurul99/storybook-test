let privateFlag;
if (process.env.PRIVATE_ACCESS_FLAG === 'true') {
  privateFlag = true;
} else if (process.env.PRIVATE_ACCESS_FLAG === 'false') {
  privateFlag = false;
} else {
  privateFlag = false;
}
const config = {
  NODE_ENV: process.env.NODE_ENV,
  HOST: process.env.REACT_HOST, //server IP where this repo hosted
  LOCAL_PORT: process.env.LOCAL_PORT || '4004', //server PORT where this repo hosted
  LOCAL_REACT_PORT: '4019',
  SSE_HOST: process.env.SSE_HOST, // Server side event host, server for pushing event to client
  PRIVATE_ACCESS_FLAG: privateFlag,
  TYPE_RESTREAMER: process.env.TYPE_RESTREAMER || 'default',

  //dependencies config
  API_IVA: process.env.API_IVA, //server IP where API for this repo hosted
  FR_DUMP: process.env.FR_DUMP, //server IP for FR dumping hosted
  LPR_DUMP: process.env.LPR_DUMP, //server IP for LPR dumping hosted
  HOST_PLAYBACK: process.env.HOST_PLAYBACK, // server IP / VMS
  ANALYTIC_SERVICE: process.env.ANALYTIC_SERVICE,

  CAM_SNAPSHOT: process.env.CAM_SNAPSHOT
};

module.exports = config;
