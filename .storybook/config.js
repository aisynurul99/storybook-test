import { addParameters, configure } from '@storybook/react';
// import { themes } from '@storybook/theming';
import defaultTheme from './defaultTheme'

function loadStories() {
  require('../stories/index.js');
  // You can require as many stories as you need.
}

addParameters({
  options: {
    theme: defaultTheme,
    showPanel: false,
  }
})

configure(loadStories, module);