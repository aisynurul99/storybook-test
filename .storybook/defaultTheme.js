import { create } from '@storybook/theming';

export default create({
  base: 'light',

  colorPrimary: 'white',
  colorSecondary: 'deepskyblue',

  // UI
  appBg: 'linear-gradient(to bottom, #1e164c, #170f33)',
  appContentBg: 'linear-gradient(to bottom, #1e164c, #170f33)',
  appBorderColor: '#1e164c',
  appBorderRadius: 4,

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: 'white',
  textInverseColor: 'rgba(255,255,255,0.9)',

  // Toolbar default and active colors
  barTextColor: 'silver',
  barSelectedColor: 'black',
  barBg: 'linear-gradient(to bottom, #1e164c, #170f33)',

  // Form colors
  inputBg: 'white',
  inputBorder: 'silver',
  inputTextColor: 'black',
  inputBorderRadius: 4,

  brandTitle: 'Storybook',
});